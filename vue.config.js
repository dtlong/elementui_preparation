const argv = require("yargs").argv;
const getFile = function () {
  const data = `
      @import "@/assets/styles/common/variables.scss";
      `;
  return data;
};
module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: getFile()
      }
    }
  },
  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "/languages",
      enableInSFC: false
    }
  },
  devServer: {
    disableHostCheck: true
  }
};
