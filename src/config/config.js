/* eslint-disable */
import { dictionaries } from "@/common/dictionaries";

export const DEFAULT_LANGUAGE = "en";
export const DICTIONARIES = {
  en: dictionaries,
  ko: dictionaries
};

export const LOCALE = {
  LOCALES: {
    en: "English",
    "zh-Hans": "繁體中文",
    "zh-Hant": "简体中文",
    cs: "Česky or Čeština",
    pl: "Polski",
    ru: "Pу́сск|ий",
    de: "Deutsch",
    fr: "Français",
    tr: "Türkçe",
    th: "ไทย",
    vi: "Tiếng Việt",
    ms: "Malay",
    no: "Norsk",
    fi: "Suomi",
    da: "Dansk",
    sv: "Svensk",
    pt: "Português",
    "pt-br": "Português (Brasil)",
    ja: "日本語",
    ko: "한국어",
    es: "Español",
    it: "Italiano",
    uk: "Ukrainian",
    hu: "Hungarian",
    ro: "Romanian",
    ar: "العربية",
    fa: "فارسی"
  }
};

export const DEV_URL = "http://192.168.1.1";
export const DEV_WS_URL = "ws://192.168.1.1";

export const SUPPORT_WS = true;
