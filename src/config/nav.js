import Routes from "./../router/routes";

const getNavBar = function() {
  let navBar = Array.from([]);
  Routes.forEach(item => {
    let newItem = {};
    if (item.show) {
      newItem = {
        name: item.name,
        text: item.text,
        path: item.path,
        opmode: item.opmode
      };
      if (item.children && item.children.length > 0) {
        let vChildren = Array.from([]);
        item.children.forEach(children => {
          if (children.show) {
            children.path = item.path + "/" + children.path;
            vChildren.push(children);
          }
        });
        if (vChildren.length > 0) {
          newItem.children = Array.from(vChildren);
        }
      }
      navBar.push(newItem);
    }
  });
  return navBar;
};

export const navList = getNavBar();
