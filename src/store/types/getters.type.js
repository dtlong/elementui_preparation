//common
export const DRAW_NAV = "drawNav";
export const LOADING = "loading";
export const ERROR_SHOW = "errorShow";
export const ERROR_MESSAGE = "errorMessage";
export const WIZARD = "wizard";

//gateway
export const IS_AUTH = "isAuth";
export const IS_PERMISSIONS = "isPermissions";
export const ABOUT = "about";
export const CONNECTIVITY = "connectivity";
export const REBOOT = "reboot";
export const DEVICE = "device";
export const POLICY = "policy";
export const CPU = "cpu";
export const MEMORY = "memory";
export const LOGIN_DATA = "loginData";
export const DEVICE_CONNECTIVITY = "deviceConnectivity";
export const DEVICE_SPECIFIC = "deviceSpecific";
export const MESH_DEVICE = "meshDevice";
export const USER_LIST = "userList";
export const CHECK_DEFAULT = "checkDefault";
export const SECURITY_LEVEL = "securityLevel";
export const FILE_BACKUP = "fileBackup";
export const POWER_SAVING_MODE = "powerSavingMode";
export const FIRMWARE_NEW_UPDATE = "firmwareNewUpdate";
export const FIRMWARE_STATUS = "firmwareStatus";
export const FACTORY_RESET = "factoryReset";
export const LED = "led";
export const UPGRADE = "upgrade";
export const UPGRADE_STATUS = "upgradeStatus";

//network
export const LAN = "lan";
export const WAN = "wan";
export const SPECIFIC_WAN = "specificWan";
export const IS_ROUTER = "isRouter";
export const CHANNEL = "channel";
export const DOCSIS = "docsis";
export const PROCEDURE = "procedure";
export const DUAL_WAN = "dualWan";
export const QMODE = "qMode";
export const PHYSICAL_INTERFACE = "physicalInterface";
export const MAC_CLONE = "macClone";
export const MOBILE_CONFIGURATION = "mobileConfiguration";
export const LAN_PORT = "lanPort";
export const DETECT_CONNECTION = "detectCOnnection";

//service
export const PORT_FILTERING = "portFiltering";
export const PORT_IP_FILTERING = "portIpFiltering";
export const PORT_FORWARDING = "portForwarding";
export const NETWORK_OPTION = "networkOption";
export const MAC_FILTERING = "macFiltering";
export const IP_FILTERING = "ipFiltering";
export const PORT_TRIGGERING = "portTriggering";
export const RESERVED_IP = "reservedIp";
export const WOL = "wol";
export const DDNS = "ddns";
export const UPNP = "upnp";
export const DMZ = "dmz";
export const ALG = "alg";
export const STATIC_ROUTE = "staticRoute";
export const RIP = "rip";
export const PING_RESULT = "pingResult";
export const TRACE_ROUTE = "traceRoute";
export const ROUTE_HOPS = "routeHops";
export const PORT_MAPPING = "portMapping";

//wifi
export const RADIO_24 = "radio24";
export const RADIO_5 = "radio5";
export const SSID_24 = "ssid24";
export const SSID_5 = "ssid5";
export const GUEST_24 = "guest24";
export const GUEST_5 = "guest5";
export const WPS = "wps";
export const ACCESS_CONTROL_24 = "accessControl24";
export const ACCESS_CONTROL_5 = "accessControl5";
export const LIST_ACCESS_24 = "listControl24";
export const LIST_ACCESS_5 = "listControl5";
export const SCAN = "scanAll";
export const SCAN_RESULT = "scanResultAll";
export const WPS_ACTIVATE = "wpsActivate";

//system
export const LANGUAGE = "language";
export const DATE_TIME = "datetime";

//security
export const FIREWALL_IPV4 = "firewallIpv4";
export const FIREWALL_IPV6 = "firewallIpv6";
export const VPN_SERVER = "vpnServer";
export const VPN_CLIENT = "vpnClient";
export const VPN_MODE = "vpnMode";
export const FILE_PATH = "filePath";
export const PARENTAL_CONTROL = "parentalControl";
export const PARENTAL_CONTROL_RULE = "parentalControlRule";
export const PARENTAL_FILTER = "parentalFilter";

//security
export const PRIVACY = "privacy";
export const TERMS = "terms";

// mediashare
export const USB = "usb";
export const FTP_SERVER = "ftpServer";
export const SAMBA_SERVER = " sambaServer";
export const DLNA_SERVER = "dlnaServer";
export const PRINTER = "printer";
export const NETWORK_FOLDER = "networkFolder";
export const CUSTOM_FOLDER = "customFolder";
export const WEBDAV_SERVER = "webdavServer";
export const TORRENT_SERVER = "torrentServer";
export const TIMEMACHINE_SERVER = "timeMachineServer";

// qos
export const QOS_CONFIGURATION = "qosConfiguration";
export const QOS_SERVICES = "qosServices";
export const IPTV = "iptv";
