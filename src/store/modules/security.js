import api from "../../api";
import {
  FIREWALL_IPV4,
  FIREWALL_IPV6,
  VPN_CLIENT,
  VPN_SERVER,
  VPN_MODE,
  FILE_PATH,
  PARENTAL_CONTROL,
  PARENTAL_CONTROL_RULE,
  PARENTAL_FILTER
} from "../types/getters.type";
import {
  FETCH_FIREWALL_IPV4,
  UPDATE_FIREWALL_IPV4,
  FETCH_FIREWALL_IPV6,
  UPDATE_FIREWALL_IPV6,
  UPDATE_FIREWALL,
  FETCH_VPN_CLIENT,
  FETCH_VPN_MODE,
  FETCH_VPN_SERVER,
  UPDATE_VPN_CLIENT,
  UPDATE_VPN_SERVER,
  DOWNLOAD_OPEN_VPN_FILE,
  UPLOAD_OPEN_VPN_FILE,
  FETCH_PARENTAL_CONTROL,
  CHANGE_PARENTAL_CONTROL,
  GRANT_PARENTAL_CONTROL,
  UPDATE_AUTH_PARENTAL_CONTROl,
  FETCH_PARENTAL_CONTROL_RULE,
  CREATE_PARENTAL_CONTROL_RULE,
  FETCH_PARENTAL_FILTER,
  UPDATE_PARENTAL_CONTROL_RULE,
  DELETE_PARENTAL_CONTROL_RULE,
  CREATE_PARENTAL_FILTER_RULE,
  DELETE_PARENTAL_FILTER_RULE
} from "../types/actions.type";
import {
  RESET_SECURITY,
  SET_FIREWALL_IPV4,
  SET_FIREWALL_IPV6,
  RESET_VPN,
  SET_VPN_CLIENT,
  SET_VPN_MODE,
  SET_VPN_SERVER,
  SET_FILE_PATH,
  SET_PARENTAL_CONTROL,
  SET_PARENTAL_CONTROL_RULE,
  SET_PARENTAL_FILTER
} from "../types/mutations.type";

const state = {
  firewallIpv4: null,
  firewallIpv6: null,
  vpnMode: null,
  vpnServer: null,
  vpnClient: null,
  filePath: null,
  parentalControl: null,
  parentalControlRule: null,
  parentalFilter: null
};

const getters = {
  [FIREWALL_IPV4](state) {
    return state.firewallIpv4;
  },
  [FIREWALL_IPV6](state) {
    return state.firewallIpv6;
  },
  [VPN_CLIENT](state) {
    return state.vpnClient;
  },
  [VPN_SERVER](state) {
    return state.vpnServer;
  },
  [VPN_MODE](state) {
    return state.vpnMode;
  },
  [FILE_PATH](state) {
    return state.filePath;
  },
  [PARENTAL_CONTROL](state) {
    return state.parentalControl;
  },
  [PARENTAL_CONTROL_RULE](state) {
    return state.parentalControlRule;
  },
  [PARENTAL_FILTER](state) {
    return state.parentalFilter;
  }
};

const actions = {
  [FETCH_FIREWALL_IPV4]({ commit }, { options }) {
    return api.security.firewall
      .firewall_ipv4_get(options)
      .then(data => commit(SET_FIREWALL_IPV4, data));
  },
  [UPDATE_FIREWALL_IPV4]({ commit }, { postData, options }) {
    return api.security.firewall
      .firewall_ipv4_update(postData, options)
      .then(() => {
        commit(RESET_SECURITY);
      });
  },
  [FETCH_FIREWALL_IPV6]({ commit }, { options }) {
    return api.security.firewall
      .firewall_ipv6_get(options)
      .then(data => commit(SET_FIREWALL_IPV6, data));
  },
  [UPDATE_FIREWALL_IPV6]({ commit }, { postData, options }) {
    return api.security.firewall
      .firewall_ipv6_update(postData, options)
      .then(() => {
        commit(RESET_SECURITY);
      });
  },
  [UPDATE_FIREWALL]({ commit }, { postData, options }) {
    return api.security.firewall.firewall_update(postData, options).then(() => {
      commit(RESET_SECURITY);
    });
  },
  [FETCH_VPN_CLIENT]({ commit }, { options }) {
    return api.security.vpn
      .vpn_client_get(options)
      .then(data => commit(SET_VPN_CLIENT, data));
  },
  [FETCH_VPN_SERVER]({ commit }, { options }) {
    return api.security.vpn
      .vpn_server_get(options)
      .then(data => commit(SET_VPN_SERVER, data));
  },
  [FETCH_VPN_MODE]({ commit }, { options }) {
    return api.security.vpn
      .vpn_mode_get(options)
      .then(data => commit(SET_VPN_MODE, data));
  },
  [UPDATE_VPN_CLIENT]({ commit }, { postData, options }) {
    return api.security.vpn.vpn_client_update(postData, options).then(() => {
      commit(SET_VPN_CLIENT, null);
      commit(SET_VPN_MODE, null);
    });
  },
  [UPDATE_VPN_SERVER]({ commit }, { postData, options }) {
    return api.security.vpn.vpn_server_update(postData, options).then(() => {
      commit(SET_VPN_SERVER, null);
      commit(SET_VPN_MODE, null);
    });
  },
  [DOWNLOAD_OPEN_VPN_FILE]({ commit }, { options }) {
    return api.security.openvpn.openvpn_download(options).then(data => {
      commit(SET_FILE_PATH, data);
    });
  },
  [UPLOAD_OPEN_VPN_FILE](_, { postData, options }) {
    return api.security.openvpn.openvpn_upload(postData, options);
  },
  [FETCH_PARENTAL_CONTROL]({ commit }, { options }) {
    return api.security.parental
      .parentalcontrol_get(options)
      .then(data => commit(SET_PARENTAL_CONTROL, data));
  },
  [CHANGE_PARENTAL_CONTROL]({ commit }, { postData, options }) {
    return api.security.parental
      .parentalcontrol_set(postData, options)
      .then(() => {
        commit(SET_PARENTAL_CONTROL, null);
      });
  },
  [GRANT_PARENTAL_CONTROL]({ commit }, { postData, options }) {
    return api.security.parental
      .parentalcontrol_ask_permission(postData, options)
      .then(() => {
        commit(SET_PARENTAL_CONTROL, null);
      });
  },
  [UPDATE_AUTH_PARENTAL_CONTROl]({ commit }, { postData, options }) {
    return api.security.parental
      .parentalcontrol_auth_update(postData, options)
      .then(() => {
        commit(SET_PARENTAL_CONTROL, null);
      });
  },
  [FETCH_PARENTAL_CONTROL_RULE]({ commit }, { options }) {
    return api.security.parental
      .parentalcontrol_rule_get(options)
      .then(data => commit(SET_PARENTAL_CONTROL_RULE, data));
  },
  [UPDATE_PARENTAL_CONTROL_RULE]({ commit }, { id, postData, options }) {
    return api.security.parental
      .parentalcontrol_update(id, postData, options)
      .then(() => commit(SET_PARENTAL_CONTROL_RULE, null));
  },
  [CREATE_PARENTAL_CONTROL_RULE]({ commit }, { postData, options }) {
    return api.security.parental
      .parentalcontrol_create(postData, options)
      .then(() => commit(SET_PARENTAL_CONTROL_RULE, null));
  },
  [DELETE_PARENTAL_CONTROL_RULE](_, { id, options }) {
    return api.security.parental
      .parentalcontrol_delete(id, options)
      .then(() => {
        const index = state.parentalControlRule.rules.findIndex(
          o => o.id === id
        );
        state.parentalControlRule.rules.splice(index, 1);
      });
  },
  [FETCH_PARENTAL_FILTER]({ commit }, { options }) {
    return api.security.parental
      .parentalcontrol_filter_get(options)
      .then(data => commit(SET_PARENTAL_FILTER, data));
  },
  [CREATE_PARENTAL_FILTER_RULE]({ commit }, { id, postData, options }) {
    return api.security.parental
      .parentalcontrol_service_create(id, postData, options)
      .then(() => {
        commit(SET_PARENTAL_FILTER, null);
      });
  },
  [DELETE_PARENTAL_FILTER_RULE]({ commit }, { categoryId, filterId, options }) {
    return api.security.parental
      .parentalcontrol_filter_delete(categoryId, filterId, options)
      .then(() => {
        commit(SET_PARENTAL_FILTER, null);
      });
  }
};

const mutations = {
  [RESET_SECURITY](state) {
    state.firewallIpv4 = null;
    state.firewallIpv6 = null;
    state.parentalControl = null;
    state.parentalControlRule = null;
    state.parentalFilter = null;
  },
  [RESET_VPN](state) {
    state.vpnClient = null;
    state.vpnMode = null;
    state.vpnServer = null;
    state.filePath = null;
  },
  [SET_FIREWALL_IPV4](state, data) {
    state.firewallIpv4 = data;
  },
  [SET_FIREWALL_IPV6](state, data) {
    state.firewallIpv6 = data;
  },
  [SET_VPN_CLIENT](state, data) {
    state.vpnClient = data;
  },
  [SET_VPN_MODE](state, data) {
    state.vpnMode = data;
  },
  [SET_VPN_SERVER](state, data) {
    state.vpnServer = data;
  },
  [SET_FILE_PATH](state, data) {
    state.filePath = data;
  },
  [SET_PARENTAL_CONTROL](state, data) {
    state.parentalControl = data;
  },
  [SET_PARENTAL_CONTROL_RULE](state, data) {
    state.parentalControlRule = data;
  },
  [SET_PARENTAL_FILTER](state, data) {
    state.parentalFilter = data;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
