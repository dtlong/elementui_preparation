import api from "../../api";
import {
  USB,
  FTP_SERVER,
  SAMBA_SERVER,
  DLNA_SERVER,
  PRINTER,
  NETWORK_FOLDER,
  CUSTOM_FOLDER,
  WEBDAV_SERVER,
  TORRENT_SERVER,
  TIMEMACHINE_SERVER
} from "../types/getters.type";
import {
  FETCH_USB,
  REMOVE_USB,
  FETCH_FTP_SERVER,
  FETCH_SAMBA_SERVER,
  UPDATE_SAMBA_SERVER,
  FETCH_DLNA_SERVER,
  UPDATE_DLNA_SERVER,
  SCAN_MEDIA_SERVER,
  FETCH_PRINTER,
  REMOVE_PRINTER,
  FETCH_NETWORK_FOLDER,
  CREATE_NETWORK_FOLDER,
  UPDATE_NETWORK_FOLDER,
  DELETE_NETWORK_FOLDER,
  FETCH_CUSTOM_FOLDER,
  UPDATE_FTP_SERVER,
  FETCH_WEBDAV_SERVER,
  UPDATE_WEBDAV_SERVER,
  FETCH_TORRENT_SERVER,
  UPDATE_TORRENT_SERVER,
  FETCH_TIMEMACHINE_SERVER,
  UPDATE_TIMEMACHINE_SERVER
} from "../types/actions.type";
import {
  SET_USB,
  RESET_MEDIASHARE,
  SET_FTP_SERVER,
  SET_SAMBA_SERVER,
  SET_DLNA_SERVER,
  SET_PRINTER,
  SET_NETWORK_FOLDER,
  SET_CUSTOM_FOLDER,
  SET_WEBDAV_SERVER,
  SET_TORRENT_SERVER,
  SET_TIMEMACHINE_SERVER
} from "../types/mutations.type";

const state = {
  usb: null,
  ftp: null,
  samba: null,
  dlna: null,
  printer: null,
  networkFolder: null,
  customFolder: null,
  webdav: null,
  torrent: null,
  timeMachine: null
};

const getters = {
  [USB](state) {
    return state.usb;
  },
  [SAMBA_SERVER](state) {
    return state.samba;
  },
  [FTP_SERVER](state) {
    return state.ftp;
  },
  [DLNA_SERVER](state) {
    return state.dlna;
  },
  [PRINTER](state) {
    return state.printer;
  },
  [NETWORK_FOLDER](state) {
    return state.networkFolder;
  },
  [CUSTOM_FOLDER](state) {
    return state.customFolder;
  },
  [WEBDAV_SERVER](state) {
    return state.webdav;
  },
  [TORRENT_SERVER](state) {
    return state.torrent;
  },
  [TIMEMACHINE_SERVER](state) {
    return state.timeMachine;
  }
};

const actions = {
  [FETCH_USB]({ commit }, { options }) {
    return api.mediashare.usb
      .usb_get(options)
      .then(data => commit(SET_USB, data));
  },
  [REMOVE_USB]({ commit }, { postData, options }) {
    return api.mediashare.usb.usb_remove(postData, options).then(() => {
      commit(SET_USB, null);
    });
  },
  [FETCH_FTP_SERVER]({ commit }, { options }) {
    return api.mediashare.ftp
      .ftp_get(options)
      .then(data => commit(SET_FTP_SERVER, data));
  },
  [UPDATE_FTP_SERVER]({ commit }, { postData, options }) {
    return api.mediashare.ftp.ftp_update(postData, options).then(() => {
      commit(SET_FTP_SERVER, null);
    });
  },
  [FETCH_SAMBA_SERVER]({ commit }, { options }) {
    return api.mediashare.samba
      .samba_get(options)
      .then(data => commit(SET_SAMBA_SERVER, data));
  },
  [UPDATE_SAMBA_SERVER]({ commit }, { postData, options }) {
    return api.mediashare.samba.samba_update(postData, options).then(() => {
      commit(SET_SAMBA_SERVER, null);
    });
  },
  [FETCH_DLNA_SERVER]({ commit }, { options }) {
    return api.mediashare.dlna
      .dlna_get(options)
      .then(data => commit(SET_DLNA_SERVER, data));
  },
  [UPDATE_DLNA_SERVER]({ commit }, { postData, options }) {
    return api.mediashare.dlna.dlna_update(postData, options).then(() => {
      commit(SET_DLNA_SERVER, null);
    });
  },
  [SCAN_MEDIA_SERVER](_, { options }) {
    return api.mediashare.dlna.dlna_scan(options);
  },
  [FETCH_PRINTER]({ commit }, { options }) {
    return api.mediashare.printer
      .printer_get(options)
      .then(data => commit(SET_PRINTER, data));
  },
  [REMOVE_PRINTER]({ commit }, { postData, options }) {
    return api.mediashare.printer
      .printer_remove({ postData, options })
      .then(() => {
        commit(SET_PRINTER, null);
      });
  },
  [FETCH_NETWORK_FOLDER]({ commit }, { options }) {
    return api.mediashare.folder
      .networkfolder_get(options)
      .then(data => commit(SET_NETWORK_FOLDER, data));
  },
  [CREATE_NETWORK_FOLDER]({ commit }, { postData, options }) {
    return api.mediashare.folder
      .networkfolder_create(postData, options)
      .then(() => {
        commit(SET_NETWORK_FOLDER, null);
      });
  },
  [UPDATE_NETWORK_FOLDER]({ commit }, { id, postData, options }) {
    return api.mediashare.folder
      .networkfolder_update(id, postData, options)
      .then(() => {
        commit(SET_NETWORK_FOLDER, null);
      });
  },
  [DELETE_NETWORK_FOLDER](_, { id, options }) {
    return api.mediashare.folder.networkfolder_delete(id, options).then(() => {
      const index = state.networkFolder.networkFolder.findIndex(
        o => o.id === id
      );
      state.networkFolder.networkFolder.splice(index, 1);
    });
  },
  [FETCH_CUSTOM_FOLDER]({ commit }, { options }) {
    return api.mediashare.custom_folder
      .customfolder_get(options)
      .then(data => commit(SET_CUSTOM_FOLDER, data));
  },
  [FETCH_WEBDAV_SERVER]({ commit }, { options }) {
    return api.mediashare.webdav
      .webdav_get(options)
      .then(data => commit(SET_WEBDAV_SERVER, data));
  },
  [UPDATE_WEBDAV_SERVER]({ commit }, { postData, options }) {
    return api.mediashare.webdav.webdav_update(postData, options).then(() => {
      commit(SET_WEBDAV_SERVER, null);
    });
  },
  [FETCH_TORRENT_SERVER]({ commit }, { options }) {
    return api.mediashare.torrent
      .torrent_get(options)
      .then(data => commit(SET_TORRENT_SERVER, data));
  },
  [UPDATE_TORRENT_SERVER]({ commit }, { postData, options }) {
    return api.mediashare.torrent.torrent_update(postData, options).then(() => {
      commit(SET_TORRENT_SERVER, null);
    });
  },
  [FETCH_TIMEMACHINE_SERVER]({ commit }, { options }) {
    return api.mediashare.timeMachine
      .timemachine_get(options)
      .then(data => commit(SET_TIMEMACHINE_SERVER, data));
  },
  [UPDATE_TIMEMACHINE_SERVER]({ commit }, { postData, options }) {
    return api.mediashare.timeMachine
      .timemachine_update(postData, options)
      .then(() => {
        commit(SET_TIMEMACHINE_SERVER, null);
      });
  }
};

const mutations = {
  [RESET_MEDIASHARE](state) {
    state.usb = null;
    state.ftp = null;
    state.samba = null;
    state.dlna = null;
    state.printer = null;
    state.customFolder = null;
    state.webdav = null;
    state.torrent = null;
    state.timeMachine = null;
  },
  [SET_USB](state, usb) {
    state.usb = usb;
  },
  [SET_FTP_SERVER](state, ftp) {
    state.ftp = ftp;
  },
  [SET_SAMBA_SERVER](state, samba) {
    state.samba = samba;
  },
  [SET_DLNA_SERVER](state, dlna) {
    state.dlna = dlna;
  },
  [SET_PRINTER](state, printer) {
    state.printer = printer;
  },
  [SET_NETWORK_FOLDER](state, networkFolder) {
    state.networkFolder = networkFolder;
  },
  [SET_CUSTOM_FOLDER](state, customFolder) {
    state.customFolder = customFolder;
  },
  [SET_WEBDAV_SERVER](state, webdav) {
    state.webdav = webdav;
  },
  [SET_TORRENT_SERVER](state, torrent) {
    state.torrent = torrent;
  },
  [SET_TIMEMACHINE_SERVER](state, timeMachine) {
    state.timeMachine = timeMachine;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
