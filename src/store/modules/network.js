import api from "../../api";
import {
  LAN,
  WAN,
  SPECIFIC_WAN,
  IS_ROUTER,
  CHANNEL,
  DOCSIS,
  PROCEDURE,
  DUAL_WAN,
  QMODE,
  PHYSICAL_INTERFACE,
  MAC_CLONE,
  MOBILE_CONFIGURATION,
  LAN_PORT,
  DETECT_CONNECTION
} from "../types/getters.type";
import {
  FETCH_LAN,
  FETCH_WAN,
  FETCH_SPECIFIC_WAN,
  UPDATE_SPECIFIC_WAN,
  FETCH_PHYSICAL_INTERFACES,
  UPDATE_PHYSICAL_INTERFACES,
  FETCH_CHANNEL,
  FETCH_DOCSIS,
  FETCH_PROCEDURE,
  UPDATE_LAN,
  FETCH_DUAL_WAN,
  FETCH_Q_MODE,
  UPDATE_DUAL_WAN,
  FETCH_MAC_CLONE,
  UPDATE_MAC_CLONE,
  UPDATE_Q_MODE,
  FETCH_MOBILE_CONFIGURATION,
  UPDATE_MOBILE_CONFIGURATION,
  FETCH_LAN_PORT,
  UPDATE_WAN_IPV6,
  UPDATE_LAN_IPV6,
  FETCH_DETECT_CONNECTION
} from "../types/actions.type";
import {
  RESET_NETWORK,
  SET_INTERFACES,
  SET_LAN,
  SET_WAN,
  SET_SPECIFIC_WAN,
  SET_CHANNEL,
  SET_DOCSIS,
  SET_PROCEDURE,
  SET_DUAL_WAN,
  SET_Q_MODE,
  SET_MAC_CLONE,
  SET_MOBILE_CONFIGURATION,
  SET_LAN_PORT,
  SET_DETECT_CONNECTION
} from "../types/mutations.type";

import Utils from "@/common/utils";
import { COOKIE_SWITCH_MODE } from "@/common/constants";

const state = {
  interfaces: null,
  lan: null,
  wan: null,
  specific_wan: [],
  switchMode: Utils.getCookie(COOKIE_SWITCH_MODE),
  channel: null,
  docsis: null,
  procedure: null,
  dual_wan: null,
  currentInterface: 0,
  qMode: null,
  macClone: null,
  mobile: null,
  lanPort: null,
  connectionDetect: null
};

const getters = {
  [LAN](state) {
    return state.lan;
  },
  [WAN](state) {
    return state.wan;
  },
  [SPECIFIC_WAN](state) {
    return state.specific_wan;
  },
  [IS_ROUTER](state) {
    let ret = true;
    //mode : bridge / router
    if (state.switchMode === "bridge") {
      ret = false;
    }
    return ret;
  },
  [CHANNEL](state) {
    return state.channel;
  },
  [DOCSIS](state) {
    return state.docsis;
  },
  [PROCEDURE](state) {
    return state.procedure;
  },
  [DUAL_WAN](state) {
    return state.dual_wan;
  },
  [QMODE](state) {
    return state.qMode;
  },
  [PHYSICAL_INTERFACE](state) {
    return state.interfaces;
  },
  [MAC_CLONE](state) {
    return state.macClone;
  },
  [MOBILE_CONFIGURATION](state) {
    return state.mobile;
  },
  [LAN_PORT](state) {
    return state.lanPort;
  },
  [DETECT_CONNECTION](state) {
    return state.connectionDetect;
  }
};

const actions = {
  [FETCH_LAN]({ commit }, { options }) {
    return api.network.lan.lan_get(options).then(data => commit(SET_LAN, data));
  },
  [FETCH_WAN]({ commit }, { options }) {
    return api.network.wan.wan_get(options).then(data => commit(SET_WAN, data));
  },
  [FETCH_DETECT_CONNECTION]({ commit, state }, { options }) {
    if (state.connectionDetect === null) {
      return api.network.wan
        .wan_connection_type_detect(options)
        .then(data => commit(SET_DETECT_CONNECTION, data));
    } else {
      return;
    }
  },
  [FETCH_SPECIFIC_WAN]({ commit }, { interfaceId, options }) {
    return api.network.wan
      .wan_get_one(interfaceId, options)
      .then(data => commit(SET_SPECIFIC_WAN, data));
  },
  [UPDATE_SPECIFIC_WAN]({ commit }, { interfaceId, postData, options }) {
    return api.network.wan
      .wan_change(interfaceId, postData, options)
      .then(() => {
        commit(SET_SPECIFIC_WAN, null);
      });
  },
  [UPDATE_WAN_IPV6](_, { interfaceId, postData, options }) {
    return api.network.wan.wan_ipv6_change(interfaceId, postData, options);
  },
  [FETCH_PHYSICAL_INTERFACES]({ commit }, { options }) {
    return api.network.physical_interfaces
      .physical_interfaces_get(options)
      .then(data => commit(SET_INTERFACES, data));
  },
  [UPDATE_PHYSICAL_INTERFACES]({ commit }, { postData, options }) {
    return api.network.physical_interfaces
      .physical_interfaces_update(postData, options)
      .then(() => commit(SET_INTERFACES, null));
  },
  [FETCH_CHANNEL]({ commit }, { options }) {
    return api.network.docsis
      .docsis_channel_get(options)
      .then(data => commit(SET_CHANNEL, data));
  },
  [FETCH_DOCSIS]({ commit }, { options }) {
    return api.network.docsis
      .docsis_get(options)
      .then(data => commit(SET_DOCSIS, data));
  },
  [FETCH_PROCEDURE]({ commit }, { options }) {
    return api.network.docsis
      .docsis_initial_procedure_get(options)
      .then(data => commit(SET_PROCEDURE, data));
  },
  [UPDATE_LAN]({ commit }, { postData, options }) {
    return api.network.lan.lan_update(postData, options).then(() => {
      commit(SET_LAN, null);
    });
  },
  [FETCH_DUAL_WAN]({ commit }, { options }) {
    return api.network.dualwan
      .dualwan_get(options)
      .then(data => commit(SET_DUAL_WAN, data));
  },
  [FETCH_Q_MODE]({ commit }, { options }) {
    return api.network.qmode
      .qmode_get(options)
      .then(data => commit(SET_Q_MODE, data));
  },
  [UPDATE_DUAL_WAN]({ commit, state }, { postData, options }) {
    return api.network.dualwan.dualwan_update(postData, options).then(() => {
      commit(SET_DUAL_WAN, null);
      state.specific_wan = [];
    });
  },
  [FETCH_MAC_CLONE]({ commit }, { interfaceId, options }) {
    return api.network.macclone
      .macclone_get(interfaceId, options)
      .then(data => commit(SET_MAC_CLONE, data));
  },
  [UPDATE_MAC_CLONE](_, { interfaceId, postData, options }) {
    return api.network.macclone.macclone_set(interfaceId, postData, options);
  },
  [UPDATE_Q_MODE]({ commit }, { postData, options }) {
    return api.network.qmode.qmode_update(postData, options).then(() => {
      commit(SET_Q_MODE, null);
    });
  },
  [FETCH_MOBILE_CONFIGURATION]({ commit }, { options }) {
    return api.network.mobile
      .mobile_configuration_get(options)
      .then(data => commit(SET_MOBILE_CONFIGURATION, data));
  },
  [UPDATE_MOBILE_CONFIGURATION]({ commit }, { postData, options }) {
    return api.network.mobile.mobile_update(postData, options).then(() => {
      commit(SET_MOBILE_CONFIGURATION, null);
    });
  },
  [FETCH_LAN_PORT]({ commit }, { options }) {
    return api.network.lan
      .lan_port_get(options)
      .then(data => commit(SET_LAN_PORT, data));
  },
  [UPDATE_WAN_IPV6](_, { interfaceId, postData, options }) {
    return api.network.wan.wan_ipv6_change(interfaceId, postData, options);
  },
  [UPDATE_LAN_IPV6](_, { postData, options }) {
    return api.network.lan.lan_ipv6_update(postData, options);
  }
};

const mutations = {
  [RESET_NETWORK](state) {
    state.interfaces = null;
    state.lan = null;
    state.wan = null;
    state.specific_wan = [];
    state.switchMode = null;
    state.channel = null;
    state.docsis = null;
    state.procedure = null;
    state.dual_wan = null;
    state.qMode = null;
    state.currentInterface = 0;
    state.macClone = null;
    state.mobile = null;
    state.lanPort = null;
    state.connectionDetect = null;
  },
  [SET_INTERFACES](state, interfaces) {
    state.interfaces = interfaces;
  },
  [SET_LAN](state, lan) {
    state.lan = lan;
  },
  [SET_WAN](state, wan) {
    state.wan = wan;
  },
  [SET_SPECIFIC_WAN](state, specific_wan) {
    if (!specific_wan) {
      state.switchMode = null;
      state.specific_wan = [];
    } else {
      state.specific_wan[specific_wan.id] = specific_wan;
      state.switchMode = specific_wan.switchMode;
      Utils.setCookie(COOKIE_SWITCH_MODE, specific_wan.switchMode);
    }
  },
  [SET_CHANNEL](state, channel) {
    state.channel = channel;
    state.channel.downstream.forEach(stream => {
      stream.errorCodeword = {};
    });
    if (channel.errorCodewords && channel.numOfDownstream > 0) {
      channel.errorCodewords.forEach((errorCodeword, index) => {
        state.channel.downstream[index].errorCodeword = errorCodeword;
      });
    }
  },
  [SET_DOCSIS](state, docsis) {
    state.docsis = docsis;
  },
  [SET_PROCEDURE](state, procedure) {
    state.procedure = procedure;
  },
  [SET_DUAL_WAN](state, dual_wan) {
    state.dual_wan = dual_wan;
  },
  [SET_Q_MODE](state, qMode) {
    state.qMode = qMode;
  },
  [SET_MAC_CLONE](state, macClone) {
    state.macClone = macClone;
  },
  [SET_MOBILE_CONFIGURATION](state, mobile) {
    state.mobile = mobile;
  },
  [SET_LAN_PORT](state, data) {
    state.lanPort = data;
  },
  [SET_DETECT_CONNECTION](state, data) {
    state.connectionDetect = data;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
