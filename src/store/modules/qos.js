import api from "../../api";
import {
  FETCH_QOS_CONFIGURATION,
  FETCH_QOS_SERVICES,
  UPDATE_QOS_CONFIGURATION,
  ADD_QOS_RULE,
  DELETE_QOS_RULE,
  CHANGE_QOS_RULE,
  FETCH_IPTV,
  UPDATE_IPTV
} from "../types/actions.type";
import {
  SET_QOS_CONFIGURATION,
  SET_QOS_SERVICES,
  SET_IPTV,
  RESET_QOS
} from "../types/mutations.type";
import { QOS_CONFIGURATION, QOS_SERVICES, IPTV } from "../types/getters.type";

const state = {
  qosConfig: null,
  qosServices: null,
  iptv: null
};

const getters = {
  [QOS_CONFIGURATION](state) {
    return state.qosConfig;
  },
  [QOS_SERVICES](state) {
    return state.qosServices;
  },
  [IPTV](state) {
    return state.iptv;
  }
};

const actions = {
  [FETCH_QOS_CONFIGURATION]({ commit }, { options }) {
    return api.qos.configuration.configuration_list(options).then(data => {
      commit(SET_QOS_CONFIGURATION, data);
    });
  },
  [FETCH_QOS_SERVICES]({ commit }, { options }) {
    return api.qos.services.service_list(options).then(data => {
      commit(SET_QOS_SERVICES, data);
    });
  },
  [UPDATE_QOS_CONFIGURATION]({ commit }, { postData, options }) {
    return api.qos.configuration
      .configuration_update(postData, options)
      .then(() => {
        commit(SET_QOS_CONFIGURATION, null);
      });
  },
  [ADD_QOS_RULE]({ commit }, { postData, options }) {
    return api.qos.configuration.rules_create(postData, options).then(() => {
      commit(SET_QOS_CONFIGURATION, null);
    });
  },
  [DELETE_QOS_RULE]({ commit }, { priorityType, index, options }) {
    return api.qos.configuration
      .rules_delete_one(priorityType, index, options)
      .then(() => {
        commit(SET_QOS_CONFIGURATION, null);
      });
  },
  [CHANGE_QOS_RULE]({ commit }, { priorityType, index, postData, options }) {
    return api.qos.configuration
      .rules_change(priorityType, index, postData, options)
      .then(() => {
        commit(SET_QOS_CONFIGURATION, null);
      });
  },
  [FETCH_IPTV]({ commit }, { options }) {
    return api.qos.iptv.iptv_get(options).then(data => {
      commit(SET_IPTV, data);
    });
  },
  [UPDATE_IPTV]({ commit }, { postData, options }) {
    return api.qos.iptv.iptv_update(postData, options).then(() => {
      commit(SET_IPTV, null);
    });
  }
};

const mutations = {
  [RESET_QOS](state) {
    state.qosConfig = null;
    state.qosServices = null;
    state.iptv = null;
  },
  [SET_QOS_CONFIGURATION](state, data) {
    state.qosConfig = data;
  },
  [SET_QOS_SERVICES](state, data) {
    state.qosServices = data;
  },
  [SET_IPTV](state, data) {
    state.iptv = data;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
