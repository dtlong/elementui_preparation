import {
  DRAW_NAV,
  LOADING,
  ERROR_SHOW,
  ERROR_MESSAGE,
  WIZARD
} from "../types/getters.type";
import {
  SET_DRAW_NAV,
  SET_LOADING,
  SET_ERROR,
  SET_WIZARD
} from "../types/mutations.type";
import {
  SHOW_LOADING,
  HIDE_LOADING,
  SHOW_ERROR,
  HIDE_ERROR,
  SHOW_WIZARD,
  HIDE_WIZARD
} from "../types/actions.type";

import { STATUS_CODE } from "@/common/constants";

const state = {
  drawNav: true,
  loading: false,
  error: false,
  errorMessage: {},
  wizard: false
};

const getters = {
  [DRAW_NAV](state) {
    return state.drawNav;
  },
  [LOADING](state) {
    return state.loading;
  },
  [ERROR_MESSAGE](state) {
    return state.errorMessage;
  },
  [ERROR_SHOW](state) {
    return state.error;
  },
  [WIZARD](state) {
    return state.wizard;
  }
};

const actions = {
  [SHOW_LOADING]({ commit }) {
    commit(SET_LOADING, true);
  },
  [HIDE_LOADING]({ commit }) {
    commit(SET_LOADING, false);
  },
  [SHOW_ERROR]({ commit }, error) {
    commit(SET_LOADING, false);
    const vError = {
      error: error,
      value: true
    };
    commit(SET_ERROR, vError);
  },
  [HIDE_ERROR]({ commit }, error) {
    const vError = {
      error: error,
      value: false
    };
    commit(SET_ERROR, vError);
  },
  [SHOW_WIZARD]({ commit }) {
    commit(SET_WIZARD, true);
  },
  [HIDE_WIZARD]({ commit }) {
    commit(SET_WIZARD, false);
  }
};

const mutations = {
  [SET_DRAW_NAV](state, toggle) {
    state.drawNav = toggle;
  },
  [SET_LOADING](state, loading) {
    state.loading = loading;
    state.error = false;
  },
  [SET_ERROR](state, data) {
    state.error = data.value;
    if (data.value) {
      if (
        data.error.response.data.error.code &&
        data.error.response.data.error.code === STATUS_CODE.EXPIRED_TOKEN
      ) {
        /* Change the message */
        state.errorMessage.translate = true;
        state.errorMessage.msg = "common_ui_dialog_auto_logged_out";
      } else {
        state.errorMessage.translate = false;
        state.errorMessage.msg = data.error.response.data.error.message;
      }
    }
  },
  [SET_WIZARD](state, value) {
    state.wizard = value;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
