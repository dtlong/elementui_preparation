import api from "../../api";
import {
  FETCH_RADIO,
  FETCH_ALL_SSID,
  UPDATE_STATUS_SSID,
  FETCH_SSID,
  FETCH_WPS,
  UPDATE_WIFI_NETWORK,
  UPDATE_SSID,
  FETCH_ACCESS_CONTROL,
  UPDATE_ACCESS_CONTROL,
  UPDATE_SPECIFIC_ACCESS_CONTROL,
  DELETE_ALL_ACCESS_CONTROL,
  DELETE_ONE_ACCESS_CONTROL,
  CREATE_ACCESS_CONTROL,
  UPDATE_STATUS_ACCESS_CONTROL,
  FETCH_RESTART_WIFI,
  FETCH_RESTORE_RADIO,
  UPDATE_RADIO,
  UPDATE_STATUS_RADIO,
  FETCH_SCAN,
  FETCH_SCAN_RESULT,
  FETCH_SCAN_SSID,
  FETCH_SCAN_SSID_RESULT,
  FETCH_ACTIVE_WPS,
  FETCH_CANCEL_WPS,
  CREATE_GUEST_SSID,
  DELETE_GUEST_SSID
} from "../types/actions.type";
import {
  RESET_WIFI,
  SET_FULL_SSID,
  SET_RADIO,
  SET_SSID,
  SET_WPS,
  SET_ACCESS_CONTROL,
  DELETE_ACCESS_CONTROL,
  SET_SCAN,
  SET_SCAN_RESULT,
  SET_WPS_ACTIVATE
} from "../types/mutations.type";
import {
  RADIO_24,
  RADIO_5,
  SSID_24,
  SSID_5,
  GUEST_24,
  GUEST_5,
  ACCESS_CONTROL_24,
  LIST_ACCESS_24,
  LIST_ACCESS_5,
  ACCESS_CONTROL_5,
  WPS,
  SCAN,
  SCAN_RESULT,
  WPS_ACTIVATE
} from "../types/getters.type";

const STATUS_OK = 200;

const state = {
  radio24: null,
  radio5: null,
  fullSsid24: null,
  fullSsid5: null,
  ssid24: null,
  ssid5: null,
  accessControl24: null,
  listAccessControl24: [],
  listAccessControl5: [],
  listSpecificSSID24: [],
  listSpecificSSID5: [],
  accessControl5: null,
  wps: null,
  currentControlSsid: null,
  scan: {
    scan24: null,
    scan5: null,
    scan2nd: null,
    scanAll: null
  },
  scanResult: {
    scan24: null,
    scan5: null,
    scan2nd: null,
    scanAll: null
  },
  wpsActivate: null,
  wpsCommon: {
    wpsCountDown: null,
    handleCountDown: null,
    connectionCheckTimer: null,
    tryCount: 0,
    showConnecting: false,
    showActivating: false,
    showCanceling: false,
    wpsStatusMessage: null
  }
};

const getters = {
  [RADIO_24](state) {
    return state.radio24;
  },
  [RADIO_5](state) {
    return state.radio5;
  },
  [SSID_24](state) {
    return state.listSpecificSSID24;
  },
  [SSID_5](state) {
    return state.listSpecificSSID5;
  },
  [GUEST_24](state) {
    let listGuest = [];
    if (state.fullSsid24) {
      state.fullSsid24.forEach(obj => {
        if (obj.index !== 0) {
          listGuest.push(obj);
        }
      });
    }
    return listGuest;
  },
  [GUEST_5](state) {
    let listGuest = [];
    if (state.fullSsid5) {
      state.fullSsid5.forEach(obj => {
        if (obj.index !== 0) {
          listGuest.push(obj);
        }
      });
    }
    return listGuest;
  },
  [WPS](state) {
    return state.wps;
  },
  [ACCESS_CONTROL_24](state) {
    return state.accessControl24;
  },
  [ACCESS_CONTROL_5](state) {
    return state.accessControl5;
  },
  [LIST_ACCESS_24](state) {
    return state.listAccessControl24;
  },
  [LIST_ACCESS_5](state) {
    return state.listAccessControl5;
  },
  [SCAN](state) {
    return state.scan;
  },
  [SCAN_RESULT](state) {
    return state.scanResult;
  },
  [WPS_ACTIVATE](state) {
    return state.wpsActivate;
  }
};

const actions = {
  [FETCH_RADIO]({ commit }, { interfaceId, options }) {
    return api.wifi.radio
      .radio_get(interfaceId, options)
      .then(data => commit(SET_RADIO, { data, interfaceId }));
  },
  [FETCH_WPS]({ commit }, { options }) {
    return api.wifi.wps.wps_get(options).then(data => commit(SET_WPS, data));
  },
  [FETCH_ALL_SSID]({ commit }, { interfaceId, options }) {
    return api.wifi.ssid
      .ssid_list_all(interfaceId, options)
      .then(data => commit(SET_FULL_SSID, { data: data, interfaceId }));
  },
  [FETCH_RESTART_WIFI]({ commit }, { interfaceId, options }) {
    return api.wifi.restart.restart(interfaceId, options).then(() => {
      commit(SET_RADIO, { data: null, interfaceId });
      commit(SET_FULL_SSID, { data: null, interfaceId });
      commit(SET_SSID, { data: null, interfaceId });
      commit(SET_ACCESS_CONTROL, { data: null, interfaceId });
    });
  },
  [FETCH_RESTORE_RADIO]({ commit }, { interfaceId, options }) {
    return api.wifi.radio
      .radio_restore_default(interfaceId, options)
      .then(() => {
        commit(SET_RADIO, { data: null, interfaceId });
        commit(SET_SSID, { data: null, interfaceId });
        commit(SET_ACCESS_CONTROL, { data: null, interfaceId });
      });
  },
  [FETCH_ACCESS_CONTROL]({ commit }, { interfaceId, ssid, options }) {
    if (interfaceId === 0) {
      return api.wifi.ssid
        .ssid_accesscontrol_get(interfaceId, ssid, options)
        .then(data => commit(SET_ACCESS_CONTROL, { data, interfaceId, ssid }));
    }
    if (interfaceId === 1) {
      return api.wifi.ssid
        .ssid_accesscontrol_get(interfaceId, ssid)
        .then(data => commit(SET_ACCESS_CONTROL, { data, interfaceId, ssid }));
    }
    return;
  },
  [FETCH_SSID]({ commit }, { interfaceId, ssid, options }) {
    if (interfaceId === 0) {
      return api.wifi.ssid
        .ssid_get(interfaceId, ssid, options)
        .then(data => commit(SET_SSID, { data: data, interfaceId, ssid }));
    } else {
      return api.wifi.ssid
        .ssid_get(interfaceId, ssid, options)
        .then(data => commit(SET_SSID, { data: data, interfaceId, ssid }));
    }
  },
  [FETCH_SCAN]({ commit }, { interfaceId, options }) {
    return api.wifi.scan
      .scan(interfaceId, options)
      .then(data => commit(SET_SCAN, { data, interfaceId }));
  },
  [FETCH_SCAN_RESULT]({ commit }, { interfaceId, options }) {
    return api.wifi.scan.scan_result(interfaceId, options).then(response => {
      if (response.status === STATUS_OK) {
        const data = response.data;
        commit(SET_SCAN_RESULT, { data, interfaceId });
      } else {
        const data = null;
        commit(SET_SCAN_RESULT, { data, interfaceId });
      }
    });
  },
  [FETCH_SCAN_SSID]({ commit }, { interfaceId, postData, options }) {
    return api.wifi.scan
      .scan_ssid(interfaceId, postData, options)
      .then(data => commit(SET_SCAN, { data, interfaceId }));
  },
  [FETCH_SCAN_SSID_RESULT]({ commit }, { interfaceId, postData, options }) {
    return api.wifi.scan
      .scan_result_ssid(interfaceId, postData, options)
      .then(response => {
        if (response.status === STATUS_OK) {
          const data = response.data;
          commit(SET_SCAN_RESULT, { data, interfaceId });
        }
      });
  },
  [UPDATE_RADIO](_, { interfaceId, postData, options }) {
    return api.wifi.radio.radio_change(interfaceId, postData, options);
  },
  [UPDATE_STATUS_RADIO](_, { interfaceId, activeData, options }) {
    return api.wifi.radio.radio_active(interfaceId, activeData, options);
  },
  [UPDATE_STATUS_SSID](_, { interfaceId, ssid, activeData, options }) {
    return api.wifi.ssid.ssid_active(interfaceId, ssid, activeData, options);
  },
  [UPDATE_SSID](_, { interfaceId, ssid, postData, options }) {
    return api.wifi.ssid.ssid_update(interfaceId, ssid, postData, options);
  },
  [UPDATE_WIFI_NETWORK](_, { interfaceId, key, postData, options }) {
    return api.wifi.network.network_type_change(
      interfaceId,
      key,
      postData,
      options
    );
  },
  [UPDATE_ACCESS_CONTROL](_, { interfaceId, ssid, postData, options }) {
    return api.wifi.ssid.ssid_accesscontrol_update(
      interfaceId,
      ssid,
      postData,
      options
    );
  },
  [CREATE_ACCESS_CONTROL](_, { interfaceId, ssid, postData, options }) {
    return api.wifi.ssid.ssid_accesscontrol_create(
      interfaceId,
      ssid,
      postData,
      options
    );
  },
  [UPDATE_SPECIFIC_ACCESS_CONTROL](
    _,
    { interfaceId, ssid, ruleId, postData, options }
  ) {
    return api.wifi.ssid.ssid_accesscontrol_patch(
      interfaceId,
      ssid,
      ruleId,
      postData,
      options
    );
  },
  [UPDATE_STATUS_ACCESS_CONTROL](
    _,
    { interfaceId, ssid, activeData, options }
  ) {
    return api.wifi.ssid.ssid_accesscontrol_active(
      interfaceId,
      ssid,
      activeData,
      options
    );
  },
  [DELETE_ALL_ACCESS_CONTROL](_, { interfaceId, ssid, options }) {
    return api.wifi.ssid.ssid_accesscontrol_delete_all(
      interfaceId,
      ssid,
      options
    );
  },
  [DELETE_ONE_ACCESS_CONTROL](
    { commit },
    { interfaceId, ssid, ruleId, options }
  ) {
    return api.wifi.ssid
      .ssid_accesscontrol_delete_one(interfaceId, ssid, ruleId, options)
      .then(() => {
        commit(SET_ACCESS_CONTROL, { data: null, interfaceId });
      });
  },
  [FETCH_ACTIVE_WPS]({ commit }, { options }) {
    return api.wifi.wps
      .wps_activate(options)
      .then(data => commit(SET_WPS_ACTIVATE, data));
  },
  [FETCH_CANCEL_WPS](_, { options }) {
    return api.wifi.wps.wps_cancel(options);
  },
  [CREATE_GUEST_SSID](_, { interfaceId, postData, options }) {
    return api.wifi.ssid.ssid_create(interfaceId, postData, options);
  },
  [DELETE_GUEST_SSID](_, { interfaceId, ssid, options }) {
    return api.wifi.ssid.ssid_guest_delete(interfaceId, ssid, options);
  }
};

const mutations = {
  [RESET_WIFI](state) {
    state.radio24 = null;
    state.radio5 = null;
    state.fullSsid24 = null;
    state.fullSsid5 = null;
    state.ssid24 = null;
    state.ssid5 = null;
    state.accessControl24 = null;
    state.listAccessControl24 = [];
    state.listAccessControl5 = [];
    state.accessControl5 = null;
    state.wps = null;
    state.currentControlSsid = null;
    state.scan = {
      scan24: null,
      scan5: null,
      scan2nd: null,
      scanAll: null
    };
    state.scanResult = {
      scan24: null,
      scan5: null,
      scan2nd: null,
      scanAll: null
    };
    state.listSpecificSSID24 = [];
    state.listSpecificSSID5 = [];
    state.wpsCommon.wpsCountDown = null;
    state.wpsCommon.handleCountDown = null;
    state.wpsCommon.connectionCheckTimer = null;
    state.wpsCommon.tryCount = 0;
    state.wpsCommon.showConnecting = false;
    state.wpsCommon.showActivating = false;
    state.wpsCommon.showCanceling = false;
    state.wpsCommon.wpsStatusMessage = null;
  },
  [SET_RADIO](state, response) {
    response.interfaceId === 0
      ? (state.radio24 = response.data)
      : (state.radio5 = response.data);
  },
  [SET_WPS](state, wps) {
    state.wps = wps;
  },
  [SET_SCAN](state, response) {
    if (response.interfaceId === 0) {
      state.scan.scan24 = response.data;
    } else if (response.interfaceId === 1) {
      state.scan.scan5 = response.data;
    } else if (response.interfaceId === 2) {
      state.scan.scan2nd = response.data;
    } else if (response.interfaceId === 99) {
      state.scan.scanAll = response.data;
    }
  },
  [SET_SCAN_RESULT](state, response) {
    if (response.interfaceId === 0) {
      state.scanResult.scan24 = response.data;
    } else if (response.interfaceId === 1) {
      state.scanResult.scan5 = response.data;
    } else if (response.interfaceId === 2) {
      state.scanResult.scan2nd = response.data;
    } else if (response.interfaceId === 99) {
      if (response.data && response.data.length > 0) {
        state.scanResult.scanAll = response.data;
      } else {
        state.scanResult.scanAll = null;
      }
    }
  },
  [SET_FULL_SSID](state, response) {
    response.interfaceId === 0
      ? (state.fullSsid24 = response.data)
      : (state.fullSsid5 = response.data);
  },
  [SET_SSID](state, response) {
    if (response.interfaceId === 0) {
      state.ssid24 = response.data;
      if (response.data !== null) {
        state.listSpecificSSID24[response.ssid] = response.data;
      } else {
        state.listSpecificSSID24[response.ssid] = null;
      }
    } else {
      state.ssid5 = response.data;
      if (response.data !== null) {
        state.listSpecificSSID5[response.ssid] = response.data;
      } else {
        state.listSpecificSSID5[response.ssid] = null;
      }
    }
  },
  [SET_ACCESS_CONTROL](state, response) {
    if (response.interfaceId === 0) {
      state.accessControl24 = response.data;
      if (response.data !== null) {
        state.listAccessControl24.push({
          active: response.data.active,
          allow: response.data.allow,
          maxRules: response.data.maxRules,
          rules: response.data.rules,
          ssid: response.ssid
        });
      } else {
        state.listAccessControl24 = [];
        state.fullSsid24 = null;
      }
    } else {
      state.accessControl5 = response.data;
      if (response.data !== null) {
        state.listAccessControl5.push({
          active: response.data.active,
          allow: response.data.allow,
          maxRules: response.data.maxRules,
          rules: response.data.rules,
          ssid: response.ssid
        });
      } else {
        state.listAccessControl5 = [];
        state.fullSsid5 = null;
      }
    }
  },
  [DELETE_ACCESS_CONTROL](state, interfaceId, ruleId) {
    if (interfaceId === 0) {
      state.accessControl24.rules = state.accessControl24.rules.filter(
        value => {
          return value.id !== ruleId;
        }
      );
    } else {
      state.accessControl5.rules = state.accessControl24.rules.filter(value => {
        return value.id !== ruleId;
      });
    }
  },
  [SET_WPS_ACTIVATE](state, data) {
    state.wpsActivate = data;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
