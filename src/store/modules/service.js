import api from "../../api";
import {
  PORT_FILTERING,
  PORT_IP_FILTERING,
  PORT_FORWARDING,
  NETWORK_OPTION,
  MAC_FILTERING,
  IP_FILTERING,
  PORT_TRIGGERING,
  RESERVED_IP,
  WOL,
  DDNS,
  UPNP,
  PORT_MAPPING,
  DMZ,
  ALG,
  RIP,
  STATIC_ROUTE,
  PING_RESULT,
  TRACE_ROUTE,
  ROUTE_HOPS
} from "../types/getters.type";
import {
  FETCH_PORT_FILTERING,
  DELETE_PORT_FILTERING,
  DELETE_ALL_PORT_FILTERING,
  CREATE_PORT_FILTERING,
  UPDATE_PORT_FILTERING,
  FETCH_PORT_IP_FILTERING,
  CREATE_PORT_IP_FILTERING,
  UPDATE_PORT_IP_FILTERING,
  DELETE_PORT_IP_FILTERING,
  FETCH_PORT_FORWARDING,
  CREATE_PORT_FORWARDING,
  UPDATE_PORT_FORWARDING,
  DELETE_PORT_FORWARDING,
  ACTIVE_PORT_FORWARDING,
  FETCH_NETWORK_OPTION,
  UPDATE_NETWORK_OPTION,
  FETCH_MAC_FILTERING,
  CREATE_MAC_FILTERING,
  DELETE_MAC_FILTERING,
  UPDATE_MAC_FILTERING,
  FETCH_IP_FILTERING,
  CREATE_IP_FILTERING,
  DELETE_IP_FILTERING,
  UPDATE_IP_FILTERING,
  FETCH_PORT_TRIGGERING,
  CREATE_PORT_TRIGGERING,
  ACTIVE_PORT_TRIGGERING,
  DELETE_PORT_TRIGGERING,
  UPDATE_PORT_TRIGGERING,
  FETCH_RESERVED_IP,
  CREATE_RESERVED_IP,
  DELETE_RESERVED_IP,
  FETCH_WOL,
  CREATE_WOL,
  DELETE_WOL_ONE,
  SEND_WOL_WAKEON,
  FETCH_DDNS,
  UPDATE_DDNS,
  FETCH_UPNP,
  UPDATE_UPNP,
  FETCH_UPNP_PORT_MAPPING,
  FETCH_DMZ,
  UPDATE_DMZ,
  FETCH_ALG,
  UPDATE_ALG,
  FETCH_RIP,
  UPDATE_RIP,
  FETCH_STATIC_ROUTE,
  CREATE_STATIC_ROUTE,
  ACTIVE_STATIC_ROUTE,
  DELETE_STATIC_ROUTE,
  UPDATE_STATIC_ROUTE,
  FETCH_PING,
  FETCH_TRACE_ROUTE,
  FETCH_ROUTE_HOPS
} from "../types/actions.type";
import {
  RESET_SERVICE,
  SET_PORT_FILTERING,
  SET_PORT_IP_FILTERING,
  SET_PORT_FORWARDING,
  SET_NETWORK_OPTION,
  SET_MAC_FILTERING,
  SET_IP_FILTERING,
  SET_PORT_TRIGGERING,
  SET_RESERVED_IP,
  SET_WOL,
  SET_DDNS,
  SET_DMZ,
  SET_UPNP,
  SET_PORT_MAPPING,
  SET_ALG,
  SET_RIP,
  SET_STATIC_ROUTE,
  SET_PING_RESULT,
  SET_TRACE_ROUTE,
  SET_ROUTE_HOPS
} from "../types/mutations.type";

const state = {
  portFiltering: null,
  portIpFiltering: null,
  portForwarding: null,
  networkOption: null,
  macFiltering: null,
  ipFiltering: null,
  portTriggering: null,
  reservedIp: null,
  wol: null,
  ddns: null,
  upnp: null,
  portMapping: null,
  dmz: null,
  alg: null,
  rip: null,
  staticRoute: null,
  pingResult: null,
  traceRoute: null,
  routeHops: null
};

const getters = {
  [PORT_FILTERING](state) {
    return state.portFiltering;
  },
  [PORT_IP_FILTERING](state) {
    return state.portIpFiltering;
  },
  [PORT_FORWARDING](state) {
    return state.portForwarding;
  },
  [NETWORK_OPTION](state) {
    return state.networkOption;
  },
  [MAC_FILTERING](state) {
    return state.macFiltering;
  },
  [IP_FILTERING](state) {
    return state.ipFiltering;
  },
  [PORT_TRIGGERING](state) {
    return state.portTriggering;
  },
  [RESERVED_IP](state) {
    return state.reservedIp;
  },
  [WOL](state) {
    return state.wol;
  },
  [DDNS](state) {
    return state.ddns;
  },
  [UPNP](state) {
    return state.upnp;
  },
  [DMZ](state) {
    return state.dmz;
  },
  [ALG](state) {
    return state.alg;
  },
  [RIP](state) {
    return state.rip;
  },
  [STATIC_ROUTE](state) {
    return state.staticRoute;
  },
  [TRACE_ROUTE](state) {
    return state.traceRoute;
  },
  [PING_RESULT](state) {
    return state.pingResult;
  },
  [ROUTE_HOPS](state) {
    return state.routeHops;
  },
  [PORT_MAPPING](state) {
    return state.portMapping;
  }
};

const actions = {
  [FETCH_PORT_FILTERING]({ commit }, { options }) {
    return api.service.port_filtering
      .portfiltering_list(options)
      .then(data => commit(SET_PORT_FILTERING, data));
  },
  [DELETE_PORT_FILTERING]({ commit }, { id, options }) {
    return api.service.port_filtering
      .portfiltering_delete_one(id, options)
      .then(() => {
        commit(SET_PORT_FILTERING, null);
      });
  },
  [DELETE_ALL_PORT_FILTERING]({ commit }, { options }) {
    return api.service.port_filtering
      .portfiltering_delete_all(options)
      .then(() => {
        commit(SET_PORT_FILTERING, null);
      });
  },
  [UPDATE_PORT_FILTERING]({ commit }, { id, postData, options }) {
    return api.service.port_filtering
      .portfiltering_patch(id, postData, options)
      .then(() => {
        commit(SET_PORT_FILTERING, null);
      });
  },
  [CREATE_PORT_FILTERING]({ commit }, { postData, options }) {
    return api.service.port_filtering
      .portfiltering_update(postData, options)
      .then(() => {
        commit(SET_PORT_FILTERING, null);
      });
  },
  [FETCH_PORT_IP_FILTERING]({ commit }, { options }) {
    return api.service.port_ip_filtering
      .port_ipfiltering_list(options)
      .then(data => commit(SET_PORT_IP_FILTERING, data));
  },
  [CREATE_PORT_IP_FILTERING]({ commit }, { postData, options }) {
    return api.service.port_ip_filtering
      .port_ipfiltering_create(postData, options)
      .then(() => {
        commit(SET_PORT_IP_FILTERING, null);
      });
  },
  [UPDATE_PORT_IP_FILTERING]({ commit }, { id, postData, options }) {
    return api.service.port_ip_filtering
      .port_ipfiltering_patch(id, postData, options)
      .then(() => {
        commit(SET_PORT_IP_FILTERING, null);
      });
  },
  [DELETE_PORT_IP_FILTERING](_, { id, options }) {
    return api.service.port_ip_filtering
      .port_ipfiltering_delete_one(id, options)
      .then(() => {
        const index = state.portIpFiltering.rules.findIndex(o => o.id === id);
        state.portIpFiltering.rules.splice(index, 1);
      });
  },

  [FETCH_PORT_FORWARDING]({ commit }, { options }) {
    return api.service.port_fowarding
      .portforwarding_list(options)
      .then(data => commit(SET_PORT_FORWARDING, data));
  },
  [CREATE_PORT_FORWARDING]({ commit }, { postData, options }) {
    return api.service.port_fowarding
      .portforwarding_create(postData, options)
      .then(() => {
        commit(SET_PORT_FORWARDING, null);
      });
  },
  [UPDATE_PORT_FORWARDING]({ commit }, { id, postData, options }) {
    return api.service.port_fowarding
      .portforwarding_patch(id, postData, options)
      .then(() => {
        commit(SET_PORT_FORWARDING, null);
      });
  },
  [DELETE_PORT_FORWARDING](_, { id, options }) {
    return api.service.port_fowarding
      .portforwarding_delete_one(id, options)
      .then(() => {
        const index = state.portForwarding.rules.findIndex(o => o.id === id);
        state.portForwarding.rules.splice(index, 1);
      });
  },
  [ACTIVE_PORT_FORWARDING]({ commit }, { postData, options }) {
    return api.service.port_fowarding
      .portforwarding_active(postData, options)
      .then(() => {
        commit(SET_PORT_FORWARDING, null);
      });
  },

  [FETCH_NETWORK_OPTION]({ commit }, { options }) {
    return api.service.misc
      .networkOption_get(options)
      .then(data => commit(SET_NETWORK_OPTION, data));
  },
  [UPDATE_NETWORK_OPTION]({ commit }, { postData, options }) {
    return api.service.misc.networkOption_update(postData, options).then(() => {
      commit(SET_NETWORK_OPTION, null);
    });
  },
  [FETCH_MAC_FILTERING]({ commit }, { options }) {
    return api.service.mac_filtering
      .macfiltering_list(options)
      .then(data => commit(SET_MAC_FILTERING, data));
  },
  [CREATE_MAC_FILTERING]({ commit }, { postData, options }) {
    return api.service.mac_filtering
      .macfiltering_create(postData, options)
      .then(() => {
        commit(SET_MAC_FILTERING, null);
      });
  },
  [DELETE_MAC_FILTERING](_, { id, options }) {
    return api.service.mac_filtering
      .macfiltering_delete_one(id, options)
      .then(() => {
        const index = state.macFiltering.rules.findIndex(o => o.id === id);
        state.macFiltering.rules.splice(index, 1);
      });
  },
  [UPDATE_MAC_FILTERING]({ commit }, { id, postData, options }) {
    return api.service.mac_filtering
      .macfiltering_patch(id, postData, options)
      .then(() => {
        commit(SET_MAC_FILTERING, null);
      });
  },
  [FETCH_IP_FILTERING]({ commit, state }, { options }) {
    if (state.ipFiltering === null) {
      return api.service.ip_filtering
        .ipfiltering_list(options)
        .then(data => commit(SET_IP_FILTERING, data));
    }
  },
  [DELETE_IP_FILTERING]({ commit }, { id, options }) {
    return api.service.ip_filtering
      .ipfiltering_delete_one(id, options)
      .then(() => {
        commit(SET_IP_FILTERING, null);
      });
  },
  [CREATE_IP_FILTERING]({ commit }, { postData, options }) {
    return api.service.ip_filtering
      .ipfiltering_create(postData, options)
      .then(() => {
        commit(SET_IP_FILTERING, null);
      });
  },
  [UPDATE_IP_FILTERING]({ commit }, { id, postData, options }) {
    return api.service.ip_filtering
      .ipfiltering_patch(id, postData, options)
      .then(() => {
        commit(SET_IP_FILTERING, null);
      });
  },
  [FETCH_PORT_TRIGGERING]({ commit }, { options }) {
    return api.service.port_triggering
      .porttriggering_list(options)
      .then(data => commit(SET_PORT_TRIGGERING, data));
  },
  [CREATE_PORT_TRIGGERING]({ commit }, { postData, options }) {
    return api.service.port_triggering
      .porttriggering_create(postData, options)
      .then(() => {
        commit(SET_PORT_TRIGGERING, null);
      });
  },
  [ACTIVE_PORT_TRIGGERING]({ commit }, { postData, options }) {
    return api.service.port_triggering
      .porttriggering_active(postData, options)
      .then(() => {
        commit(SET_PORT_TRIGGERING, null);
      });
  },
  [UPDATE_PORT_TRIGGERING]({ commit }, { id, postData, options }) {
    return api.service.port_triggering
      .porttriggering_patch(id, postData, options)
      .then(() => {
        commit(SET_PORT_TRIGGERING, null);
      });
  },
  [DELETE_PORT_TRIGGERING](_, { id, options }) {
    return api.service.port_triggering
      .porttriggering_delete_one(id, options)
      .then(() => {
        const index = state.portTriggering.rules.findIndex(o => o.id === id);
        state.portTriggering.rules.splice(index, 1);
      });
  },
  [FETCH_RESERVED_IP]({ commit, state }, { options }) {
    if (state.reservedIp === null) {
      return api.service.reserved_ip.reservedip_list(options).then(data => {
        commit(SET_RESERVED_IP, data);
      });
    }
  },
  [CREATE_RESERVED_IP]({ commit }, { postData, options }) {
    return api.service.reserved_ip
      .reservedip_create(postData, options)
      .then(() => commit(SET_RESERVED_IP, null));
  },
  [DELETE_RESERVED_IP](_, { id, options }) {
    return api.service.reserved_ip
      .reservedip_delete_one(id, options)
      .then(() => {
        const index = state.reservedIp.rules.findIndex(o => o.id === id);
        state.reservedIp.rules.splice(index, 1);
      });
  },
  [FETCH_WOL]({ commit, state }, { options }) {
    if (state.wol === null) {
      return api.service.wol.wol_list(options).then(data => {
        commit(SET_WOL, data);
      });
    }
  },
  [CREATE_WOL]({ commit }, { postData, options }) {
    return api.service.wol
      .wol_create(postData, options)
      .then(() => commit(SET_WOL, null));
  },
  [DELETE_WOL_ONE](_, { id, options }) {
    return api.service.wol.wol_delete_one(id, options).then(() => {
      const index = state.wol.rules.findIndex(o => o.id === id);
      state.wol.rules.splice(index, 1);
    });
  },
  [SEND_WOL_WAKEON](_, { postData, options }) {
    return api.service.wol.wol_wakeon(postData, options);
  },
  [FETCH_DDNS]({ commit }, { options }) {
    return api.service.ddns
      .ddns_get(options)
      .then(data => commit(SET_DDNS, data));
  },
  [UPDATE_DDNS]({ commit }, { postData, options }) {
    return api.service.ddns
      .ddns_update(postData, options)
      .then(() => commit(SET_DDNS, null));
  },
  [FETCH_DMZ]({ commit }, { options }) {
    return api.service.dmz.dmz_get(options).then(data => commit(SET_DMZ, data));
  },
  [UPDATE_DMZ]({ commit }, { postData, options }) {
    return api.service.dmz
      .dmz_update(postData, options)
      .then(() => commit(SET_DMZ, null));
  },
  [FETCH_UPNP]({ commit }, { options }) {
    return api.service.upnp
      .upnp_get(options)
      .then(data => commit(SET_UPNP, data));
  },
  [UPDATE_UPNP]({ commit }, { postData, options }) {
    return api.service.upnp
      .upnp_update(postData, options)
      .then(() => commit(SET_UPNP, null));
  },
  [FETCH_UPNP_PORT_MAPPING]({ commit }, { options }) {
    return api.service.upnp
      .upnp_portmapping_get(options)
      .then(data => commit(SET_PORT_MAPPING, data));
  },
  [FETCH_ALG]({ commit }, { options }) {
    return api.service.alg.alg_get(options).then(data => commit(SET_ALG, data));
  },
  [UPDATE_ALG]({ commit }, { postData, options }) {
    return api.service.alg
      .alg_update(postData, options)
      .then(() => commit(SET_ALG, null));
  },
  [FETCH_STATIC_ROUTE]({ commit }, { options }) {
    return api.service.static_route
      .staticroute_list(options)
      .then(data => commit(SET_STATIC_ROUTE, data));
  },
  [CREATE_STATIC_ROUTE]({ commit }, { postData, options }) {
    return api.service.static_route
      .staticroute_create(postData, options)
      .then(() => {
        commit(SET_STATIC_ROUTE, null);
      });
  },
  [ACTIVE_STATIC_ROUTE]({ commit }, { postData, options }) {
    return api.service.static_route
      .staticroute_active(postData, options)
      .then(() => {
        commit(SET_STATIC_ROUTE, null);
      });
  },
  [UPDATE_STATIC_ROUTE]({ commit }, { id, postData, options }) {
    return api.service.static_route
      .staticroute_patch(id, postData, options)
      .then(() => {
        commit(SET_STATIC_ROUTE, null);
      });
  },
  [DELETE_STATIC_ROUTE]({ state }, { id, options }) {
    return api.service.static_route
      .staticroute_delete_one(id, options)
      .then(() => {
        const index = state.staticRoute.list.findIndex(o => o.id === id);
        state.staticRoute.list.splice(index, 1);
      });
  },
  [FETCH_RIP]({ commit }, { options }) {
    return api.service.rip.rip_get(options).then(data => commit(SET_RIP, data));
  },
  [UPDATE_RIP]({ commit }, { postData, options }) {
    return api.service.rip
      .rip_update(postData, options)
      .then(() => commit(SET_RIP, null));
  },
  [FETCH_PING]({ commit }, { postData, options }) {
    return api.service.diagnostics
      .diagnostics_ping(postData, options)
      .then(data => commit(SET_PING_RESULT, data));
  },
  [FETCH_TRACE_ROUTE]({ commit }, { postData, options }) {
    return api.service.diagnostics
      .diagnostics_traceroute(postData, options)
      .then(data => commit(SET_TRACE_ROUTE, data));
  },
  [FETCH_ROUTE_HOPS]({ commit }, { options }) {
    return api.service.diagnostics
      .diagnostics_routehops(options)
      .then(data => commit(SET_ROUTE_HOPS, data));
  }
};

const mutations = {
  [RESET_SERVICE](state) {
    state.portFiltering = null;
    state.portIpFiltering = null;
    state.portForwarding = null;
    state.networkOption = null;
    state.macFiltering = null;
    state.ipFiltering = null;
    state.portTriggering = null;
    state.reservedIp = null;
    state.wol = null;
    state.ddns = null;
    state.dmz = null;
    state.upnp = null;
    state.alg = null;
    state.rip = null;
    state.staticRoute = null;
    state.pingResult = null;
    state.traceRoute = null;
    state.routeHops = null;
    state.portMapping = null;
  },
  [SET_PORT_FILTERING](state, portFiltering) {
    state.portFiltering = portFiltering;
  },
  [SET_PORT_IP_FILTERING](state, portIpFiltering) {
    state.portIpFiltering = portIpFiltering;
  },
  [SET_PORT_FORWARDING](state, portForwarding) {
    state.portForwarding = portForwarding;
  },
  [SET_NETWORK_OPTION](state, networkOption) {
    state.networkOption = networkOption;
  },
  [SET_MAC_FILTERING](state, macFiltering) {
    state.macFiltering = macFiltering;
  },
  [SET_IP_FILTERING](state, ipFiltering) {
    state.ipFiltering = ipFiltering;
  },
  [SET_PORT_TRIGGERING](state, portTriggering) {
    state.portTriggering = portTriggering;
  },
  [SET_RESERVED_IP](state, reservedIp) {
    state.reservedIp = reservedIp;
  },
  [SET_WOL](state, wol) {
    state.wol = wol;
  },
  [SET_DDNS](state, ddns) {
    state.ddns = ddns;
  },
  [SET_DMZ](state, dmz) {
    state.dmz = dmz;
  },
  [SET_UPNP](state, upnp) {
    state.upnp = upnp;
  },
  [SET_PORT_MAPPING](state, data) {
    state.portMapping = data;
  },
  [SET_ALG](state, alg) {
    state.alg = alg;
  },
  [SET_RIP](state, rip) {
    state.rip = rip;
  },
  [SET_STATIC_ROUTE](state, data) {
    state.staticRoute = data;
  },
  [SET_PING_RESULT](state, data) {
    state.pingResult = data;
  },
  [SET_TRACE_ROUTE](state, data) {
    state.traceRoute = data;
  },
  [SET_ROUTE_HOPS](state, data) {
    state.routeHops = data;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
