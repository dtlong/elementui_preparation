import api from "../../api";
import { PRIVACY, TERMS } from "../types/getters.type";
import { FETCH_PRIVACY, FETCH_TERMS } from "../types/actions.type";
import { SET_PRIVACY, SET_TERMS } from "../types/mutations.type";

const state = {
  privacy: null,
  terms: null
};

const getters = {
  [PRIVACY](state) {
    return state.privacy;
  },
  [TERMS](state) {
    return state.terms;
  }
};

const actions = {
  [FETCH_PRIVACY]({ commit }, { countryCode, options }) {
    return api.privacy.legaldoc
      .legaldoc_privacy_get(countryCode, options)
      .then(data => commit(SET_PRIVACY, data));
  },
  [FETCH_TERMS]({ commit }, { countryCode, options }) {
    return api.privacy.legaldoc
      .legaldoc_terms_get(countryCode, options)
      .then(data => commit(SET_TERMS, data));
  }
};

const mutations = {
  [SET_PRIVACY](state, data) {
    state.privacy = data;
  },
  [SET_TERMS](state, data) {
    state.terms = data;
  }
};

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
};
