import Vue from "vue";
import Vuex from "vuex";
import common from "./modules/common";
import gateway from "./modules/gateway";
import network from "./modules/network";
import service from "./modules/service";
import wifi from "./modules/wifi";
import security from "./modules/security";
import privacy from "./modules/privacy";
import mediashare from "./modules/mediashare";
import qos from "./modules/qos";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    common,
    gateway,
    network,
    service,
    wifi,
    security,
    privacy,
    mediashare,
    qos
  }
});

export default store;
