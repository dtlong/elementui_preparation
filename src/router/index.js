import Vue from "vue";
import Router from "vue-router";
import Routes from "./routes";
import Utils from "@/common/utils";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: Routes.map(route => ({
    path: route.path,
    name: route.name,
    component: route.component,
    redirect: route.redirect,
    children: route.children
  }))
});
