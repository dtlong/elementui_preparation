import Login from "@/components/common/Login/Login";
import VHome from "@/views/Home/VHome";
import VNetwork from "@/views/Network/VNetwork";
import VInternet from "@/views/Network/VInternet";
import VFiltering from "@/views/Security/VFiltering";
import VSecurity from "@/views/Security/VSecurity";
import Utils from "@/common/utils";
import { QMODE_OPERATION, COOKIE_OPMODE, APP_URL } from "@/common/constants";

const getOPMode = () => {
  let vOPMode = Utils.getCookie(COOKIE_OPMODE);
  if (vOPMode === undefined || vOPMode === null) {
    vOPMode = QMODE_OPERATION.ROUTER;
  }
  return vOPMode;
};
import VWireless from "@/views/Wireless/VWireless";
import VGuestNetwork from "@/views/Wireless/VGuestNetwork";
const routes = [
  {
    path: "/",
    name: "login",
    component: Login,
    text: "login",
    show: false
  },
  {
    path: "/home",
    name: "home",
    component: VHome,
    text: "text_menu_home",
    show: true
  },
  {
    path: "/network",
    name: "network",
    text: "text_menu_network",
    component: VNetwork,
    show: true,
    redirect: "/network/internet",
    children: [
      {
        path: "internet",
        name: "internet",
        text: "text_submenu_internet",
        component: VInternet,
        show: true
      }
    ]
  },
  {
    path: APP_URL.SECURITY.url,
    name: "vsecurity",
    text: "text_menu_security",
    component: VSecurity,
    redirect: APP_URL.SECURITY_FILTERING.url,
    opmode: [],
    show: true,
    children: [

      {
        path: APP_URL.SECURITY_FILTERING.path,
        text: "text_submenu_filtering",
        name: "filtering",
        component: VFiltering,
        show: true,
        opmode: []
      }
    ]
  },
  {
    path: "/wireless",
    name: "wireless",
    text: "text_menu_wireless",
    component: VWireless,
    show: true,
    redirect: "/wireless/guest",
    children: [
      {
        path: APP_URL.WIRELESS_GUEST.path,
        name: "guest",
        text: "text_submenu_guest_network",
        component: VGuestNetwork,
        show: true
      }
    ]
  }
];

export default routes;
