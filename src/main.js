import "@/plugins/axios";

import Vue from "vue";
import App from "./App.vue";
import Router from "./router";
import Store from "@/store";
import I18n from "@/plugins/i18n";
import Cookie from "vue-cookie";
import ElementUI from "element-ui";
import { COOKIE_LANGUAGE } from "@/common/constants";
import VeeValidate from "vee-validate";
import Utils from "@/common/utils";
import * as config from "@/config/config";
/* Overide the styles */
import "./assets/styles/common/element-variables.scss";

let defaultLanguage;
if (config) {
  defaultLanguage = config.DEFAULT_LANGUAGE;
}

Vue.config.productionTip = false;
Vue.use(Cookie);
let vLanguage = Utils.getCookie(COOKIE_LANGUAGE);
if (vLanguage === null || vLanguage === undefined) {
  vLanguage = defaultLanguage;
  Utils.setCookie(COOKIE_LANGUAGE, vLanguage);
}
I18n.locale = vLanguage;
Vue.use(ElementUI, {
  i18n: (key, value) => I18n.t(key, value)
});

let dictionaries;
if (config) {
  dictionaries = config.DICTIONARIES;
}

Vue.use(VeeValidate, {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: dictionaries,
  errorBagName: "errors", // change if property conflicts
  events: "change|blur|input|click",
  fieldsBagName: "veeValidatefields",
  i18n: I18n, // the vue-i18n plugin instance
  i18nRootKey: "validations", // the nested key under which the validation messages will be located
  inject: true,
  validity: false
});

Vue.prototype.$eventBus = new Vue();

new Vue({
  router: Router,
  store: Store,
  i18n: I18n,
  cookie: Cookie,
  render: h => h(App)
}).$mount("#app");
