import { request } from "./base";

const port_filtering = {
  portfiltering_list(options) {
    return request("get", "/service/portFiltering", {}, options);
  },
  portfiltering_update(postData, options) {
    return request("post", `/service/portFiltering`, postData, options);
  },
  portfiltering_patch(ruleId, postData, options) {
    return request(
      "put",
      `/service/portFiltering/${ruleId}`,
      postData,
      options
    );
  },
  portfiltering_delete_one(ruleId, options) {
    return request("delete", `/service/portFiltering/${ruleId}`, {}, options);
  },
  portfiltering_delete_all(options) {
    return request("post", "/service/portFiltering/deletes", {}, options);
  }
};

const port_fowarding = {
  portforwarding_list(options) {
    return request("get", "/service/portForwarding", {}, options);
  },
  portforwarding_create(postData, options) {
    return request("post", "/service/portForwarding", postData, options);
  },
  portforwarding_patch(id, postData, options) {
    return request("put", `/service/portForwarding/${id}`, postData, options);
  },
  portforwarding_delete_one(id, options) {
    return request("post", `/service/portForwarding/${id}`, {}, options);
  },
  portforwarding_active(postData, options) {
    return request("post", `/service/portForwarding/active`, postData, options);
  }
};

const port_ip_filtering = {
  port_ipfiltering_list(options) {
    return request("get", "/service/portIpFiltering", {}, options);
  },
  port_ipfiltering_create(postData, options) {
    return request("post", "/service/portIpFiltering", postData, options);
  },
  port_ipfiltering_patch(id, postData, options) {
    return request("put", `/service/portIpFiltering/${id}`, postData, options);
  },
  port_ipfiltering_delete_one(id, options) {
    return request("delete", `/service/portIpFiltering/${id}`, {}, options);
  }
};

const misc = {
  networkOption_get(options) {
    return request("get", "/service/networkOption", {}, options);
  },
  networkOption_update(postData, options) {
    return request("put", "/service/networkOption", postData, options);
  }
};

const mac_filtering = {
  macfiltering_list(options) {
    return request("get", "/service/macFiltering", {}, options);
  },
  macfiltering_create(postData, options) {
    return request("post", "/service/macFiltering", postData, options);
  },
  macfiltering_delete_one(id, options) {
    return request("delete", `/service/macFiltering/${id}`, {}, options);
  },
  macfiltering_patch(id, postData, options) {
    return request("put", `/service/macFiltering/${id}`, postData, options);
  }
};

const ip_filtering = {
  ipfiltering_list(options) {
    return request("get", "/service/ipFiltering", {}, options);
  },
  ipfiltering_create(postData, options) {
    return request("post", "/service/ipFiltering", postData, options);
  },
  ipfiltering_delete_one(id, options) {
    return request("delete", `/service/ipFiltering/${id}`, {}, options);
  },
  ipfiltering_patch(id, postData, options) {
    return request("put", `/service/ipFiltering/${id}`, postData, options);
  }
};

const port_triggering = {
  porttriggering_list(options) {
    return request("get", "/service/portTriggering", {}, options);
  },
  porttriggering_create(postData, options) {
    return request("post", "/service/portTriggering", postData, options);
  },
  porttriggering_active(postData, options) {
    return request("post", "/service/portTriggering/active", postData, options);
  },
  porttriggering_delete_one(id, options) {
    return request("delete", `/service/portTriggering/${id}`, {}, options);
  },
  porttriggering_patch(id, postData, options) {
    return request("put", `/service/portTriggering/${id}`, postData, options);
  }
};

const reserved_ip = {
  reservedip_list(options) {
    return request("get", "/service/reservedIP", {}, options);
  },
  reservedip_create(postData, options) {
    return request("post", "/service/reservedIP", postData, options);
  },
  reservedip_delete_one(id, options) {
    return request("delete", `/service/reservedIP/${id}`, {}, options);
  }
};

const wol = {
  wol_list(options) {
    return request("get", "/service/wol", {}, options);
  },
  wol_create(postData, options) {
    return request("post", "/service/wol", postData, options);
  },
  wol_delete_one(id, options) {
    return request("delete", `/service/wol/${id}`, {}, options);
  },
  wol_wakeon(postData, options) {
    return request("post", "/service/wol/wakeon", postData, options);
  }
};

const ddns = {
  ddns_get(options) {
    return request("get", "/service/ddns", {}, options);
  },
  ddns_update(postData, options) {
    return request("put", "/service/ddns", postData, options);
  }
};

const upnp = {
  upnp_get(options) {
    return request("get", "/service/upnp", {}, options);
  },
  upnp_update(postData, options) {
    return request("put", "/service/upnp", postData, options);
  },
  upnp_portmapping_get(options) {
    return request("get", "/service/upnp/portmapping", {}, options);
  }
};

const dmz = {
  dmz_get(options) {
    return request("get", "/service/dmz", {}, options);
  },
  dmz_update(postData, options) {
    return request("put", "/service/dmz", postData, options);
  }
};

const alg = {
  alg_get(options) {
    return request("get", "/service/alg", options);
  },
  alg_update(postData, options) {
    return request("put", "/service/alg", postData, options);
  }
};

const rip = {
  rip_get(options) {
    return request("get", "/service/rip", {}, options);
  },
  rip_update(postData, options) {
    return request("put", "/service/rip", postData, options);
  }
};

const static_route = {
  staticroute_list(options) {
    return request("get", "/service/staticRoute", {}, options);
  },
  staticroute_create(postData, options) {
    return request("post", "/service/staticRoute", postData, options);
  },
  staticroute_active(postData, options) {
    return request("post", "/service/staticRoute/active", postData, options);
  },
  staticroute_delete_one(id, options) {
    return request("delete", `/service/staticRoute/${id}`, {}, options);
  },
  staticroute_patch(id, postData, options) {
    return request("put", `/service/staticRoute/${id}`, postData, options);
  }
};

const diagnostics = {
  diagnostics_ping(postData, options) {
    return request("post", "/service/diagnostics/ping", postData, options);
  },
  diagnostics_traceroute(postData, options) {
    return request(
      "post",
      "/service/diagnostics/traceroute",
      postData,
      options
    );
  },
  diagnostics_routehops(options) {
    return request("post", "/service/diagnostics/routehops", {}, options);
  }
};

export default {
  port_filtering,
  port_fowarding,
  port_ip_filtering,
  misc,
  mac_filtering,
  ip_filtering,
  port_triggering,
  reserved_ip,
  wol,
  ddns,
  upnp,
  dmz,
  alg,
  rip,
  static_route,
  diagnostics
};
