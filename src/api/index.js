import gateway from "./gateway";
import network from "./network";
import wifi from "./wifi";
import service from "./service";
import security from "./security";
import privacy from "./privacy";
import mediashare from "./mediashare";
import qos from "./qos";

export default {
  gateway,
  network,
  wifi,
  service,
  security,
  privacy,
  mediashare,
  qos
};
