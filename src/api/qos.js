import { request } from "./base";

const configuration = {
  configuration_list(options) {
    return request("get", "/qos/configuration", {}, options);
  },
  configuration_update(postData, options) {
    return request("put", "/qos/configuration", postData, options);
  },
  rules_create(postData, options) {
    return request("post", "/qos/rules", postData, options);
  },
  rules_change(priorityType, index, postData, options) {
    return request(
      "put",
      `/qos/rules/${priorityType}/${index}`,
      postData,
      options
    );
  },
  rules_delete_one(priorityType, index, options) {
    return request(
      "delete",
      `/qos/rules/${priorityType}/${index}`,
      {},
      options
    );
  }
};

const services = {
  service_list() {
    return request("get", "/qos/service");
  }
};

const iptv = {
  iptv_get() {
    return request("get", "/qos/iptv");
  },
  iptv_update(postData) {
    return request("put", "/qos/iptv", postData);
  }
};

export default {
  configuration,
  services,
  iptv
};
