import { request } from "./base";

const legaldoc = {
  legaldoc_privacy_get(countryCode, options) {
    return request("get", `/cpe/legaldoc/privacy/${countryCode}`, {}, options);
  },
  legaldoc_terms_get(countryCode, options) {
    return request("get", `/cpe/legaldoc/terms/${countryCode}`, {}, options);
  }
};

export default {
  legaldoc
};
