import { request } from "./base";

const usb = {
  usb_get(options) {
    return request("get", "/mediashare/usb", {}, options);
  },
  usb_remove(postdata, options) {
    return request("post", "/mediashare/usb/remove", postdata, options);
  }
};

const ftp = {
  ftp_get(options) {
    return request("get", "/mediashare/ftp", {}, options);
  },
  ftp_update(propsData, options) {
    return request("put", "/mediashare/ftp", propsData, options);
  }
};

const samba = {
  samba_get(options) {
    return request("get", "/mediashare/samba ", {}, options);
  },
  samba_update(propsData, options) {
    return request("put", "/mediashare/samba", propsData, options);
  }
};

const dlna = {
  dlna_get(options) {
    return request("get", "/mediashare/dlna", {}, options);
  },
  dlna_update(propsData, options) {
    return request("put", "/mediashare/dlna", propsData, options);
  },
  dlna_scan(options) {
    return request("post", "/mediashare/dlna/scan", {}, options);
  }
};

const printer = {
  printer_get(options) {
    return request("get", "/mediashare/printer", {}, options);
  },
  printer_remove(postdata, options) {
    return request("post", "/mediashare/printer/remove", postdata, options);
  }
};

const folder = {
  networkfolder_get(options) {
    return request("get", "/mediashare/networkFolder", {}, options);
  },
  networkfolder_delete(id, options) {
    return request("delete", `/mediashare/networkFolder/${id}`, {}, options);
  },
  networkfolder_update(id, postdata, options) {
    return request("put", `/mediashare/networkFolder/${id}`, postdata, options);
  },
  networkfolder_create(postdata, options) {
    return request("post", "/mediashare/networkFolder", postdata, options);
  }
};

const custom_folder = {
  customfolder_get(options) {
    return request("get", "/mediashare/customFolder", {}, options);
  }
};

const webdav = {
  webdav_get(options) {
    return request("get", "/mediashare/webdav", {}, options);
  },
  webdav_update(postData, options) {
    return request("put", "/mediashare/webdav", postData, options);
  }
};

const torrent = {
  torrent_get(options) {
    return request("get", "/mediashare/torrent", {}, options);
  },
  torrent_update(postData, options) {
    return request("put", "/mediashare/torrent", postData, options);
  }
};

const timeMachine = {
  timemachine_get(options) {
    return request("get", "/mediashare/timemachine", {}, options);
  },
  timemachine_update(postData, options) {
    return request("put", "/mediashare/timemachine", postData, options);
  }
};

export default {
  usb,
  ftp,
  samba,
  dlna,
  printer,
  folder,
  custom_folder,
  webdav,
  torrent,
  timeMachine
};
