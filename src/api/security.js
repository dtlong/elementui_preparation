import { request } from "./base";

const firewall = {
  firewall_get(options) {
    return request("get", "/security/firewall", {}, options);
  },
  firewall_update(postData, options) {
    return request("put", "/security/firewall", postData, options);
  },
  firewall_ipv4_get(options) {
    return request("get", "/security/firewall/ipv4", {}, options);
  },
  firewall_ipv4_update(postData, options) {
    return request("put", "/security/firewall/ipv4", postData, options);
  },
  firewall_ipv6_get(options) {
    return request("get", "/security/firewall/ipv6", {}, options);
  },
  firewall_ipv6_update(postData, options) {
    return request("put", "/security/firewall/ipv6", postData, options);
  }
};

const vpn = {
  vpn_mode_get(options) {
    return request("get", "/security/vpn", {}, options);
  },
  vpn_server_get(options) {
    return request("get", "/security/vpn/server", {}, options);
  },
  vpn_server_update(postData, options) {
    return request("put", "/security/vpn/server", postData, options);
  },
  vpn_client_get(options) {
    return request("get", "/security/vpn/client", {}, options);
  },
  vpn_client_update(postData, options) {
    return request("put", "/security/vpn/client", postData, options);
  }
};

const openvpn = {
  openvpn_download(options) {
    return request("post", "/security/openvpn/download", {}, options);
  },
  openvpn_upload(postFile, options) {
    return request("post", "/security/openvpn/upload", postFile, options);
  }
};

const parental = {
  parentalcontrol_get(options) {
    return request("get", "/security/parentalControl", {}, options);
  },
  parentalcontrol_set(postData, options) {
    return request("put", "/security/parentalControl", postData, options);
  },
  parentalcontrol_ask_permission(postData, options) {
    return request(
      "post",
      "/security/parentalControl/grant",
      postData,
      options
    );
  },
  parentalcontrol_auth_update(postData, options) {
    return request("put", "/security/parentalControl/auth", postData, options);
  },
  parentalcontrol_rule_get(options) {
    return request("get", "/security/parentalControl/rule", {}, options);
  },
  parentalcontrol_filter_get(options) {
    return request("get", "/security/parentalControl/filter", {}, options);
  },
  parentalcontrol_update(id, postData, options) {
    return request("put", `/security/parentalControl/${id}`, postData, options);
  },
  parentalcontrol_create(postData, options) {
    return request("post", `/security/parentalControl`, postData, options);
  },
  parentalcontrol_delete(id, options) {
    return request("delete", `/security/parentalControl/${id}`, {}, options);
  },
  parentalcontrol_service_create(id, postData, options) {
    return request(
      "post",
      `/security/parentalControl/filter/${id}`,
      postData,
      options
    );
  },
  parentalcontrol_filter_delete(categoryId, filterId, options) {
    return request(
      "delete",
      `/security/parentalControl/filter/${categoryId}/${filterId}`,
      {},
      options
    );
  }
};

export default {
  firewall,
  vpn,
  openvpn,
  parental
};
