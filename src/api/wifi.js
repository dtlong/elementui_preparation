import { request } from "./base";

const radio = {
  radio_get(interfaceId, options) {
    return request("get", `/wifi/${interfaceId}/radio`, {}, options);
  },
  radio_restore_default(interfaceId, options) {
    return request(
      "post",
      `/wifi/${interfaceId}/radio/restoreDefault`,
      {},
      options
    );
  },
  radio_change(interfaceId, postData, options) {
    return request("put", `/wifi/${interfaceId}/radio`, postData, options);
  },
  radio_active(interfaceId, activeData, options) {
    return request(
      "post",
      `/wifi/${interfaceId}/radio/active`,
      activeData,
      options
    );
  }
};

const wps = {
  wps_get(options) {
    return request("get", `/wifi/wps`, {}, options);
  },
  wps_activate(options) {
    return request("post", `/wifi/wps/activate`, {}, options);
  },
  wps_cancel(options) {
    return request("post", `/wifi/wps/cancel`, {}, options);
  }
};

const ssid = {
  ssid_list_all(interfaceId, options) {
    return request("get", `/wifi/${interfaceId}/ssid`, {}, options);
  },
  ssid_get(interfaceId, ssid, options) {
    return request("get", `/wifi/${interfaceId}/ssid/${ssid}`, {}, options);
  },
  ssid_active(interfaceId, ssid, activeData, options) {
    return request(
      "post",
      `/wifi/${interfaceId}/ssid/${ssid}/active`,
      activeData,
      options
    );
  },
  ssid_update(interfaceId, ssid, postData, options) {
    return request(
      "put",
      `/wifi/${interfaceId}/ssid/${ssid}`,
      postData,
      options
    );
  },
  ssid_accesscontrol_get(interfaceId, ssid, options) {
    return request(
      "get",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl`,
      {},
      options
    );
  },
  ssid_accesscontrol_update(interfaceId, ssid, postData, options) {
    return request(
      "put",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl`,
      postData,
      options
    );
  },
  ssid_accesscontrol_patch(interfaceId, ssid, ruleId, postData, options) {
    return request(
      "put",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl/${ruleId}`,
      postData,
      options
    );
  },
  ssid_accesscontrol_active(interfaceId, ssid, activeData, options) {
    return request(
      "post",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl/active`,
      activeData,
      options
    );
  },
  ssid_accesscontrol_create(interfaceId, ssid, postData, options) {
    return request(
      "post",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl`,
      postData,
      options
    );
  },
  ssid_accesscontrol_delete_one(interfaceId, ssid, ruleId, options) {
    return request(
      "delete",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl/${ruleId}`,
      {},
      options
    );
  },
  ssid_accesscontrol_delete_all(interfaceId, ssid, options) {
    return request(
      "post",
      `/wifi/${interfaceId}/ssid/${ssid}/accessControl/deletes`,
      {},
      options
    );
  },
  ssid_create(interfaceId, postData, options) {
    return request("post", `/wifi/${interfaceId}/ssid`, postData, options);
  },
  ssid_guest_delete(interfaceId, ssid, options) {
    return request("delete", `/wifi/${interfaceId}/ssid/${ssid}`, {}, options);
  }
};

const restart = {
  restart(interfaceId, options) {
    return request("post", `/wifi/${interfaceId}/restart`, {}, options);
  }
};

const network = {
  network_type_change(interfaceId, key, postData, options) {
    return request(
      "put",
      `/wifi/${interfaceId}/networkType/${key}`,
      postData,
      options
    );
  }
};

const scan = {
  scan(interfaceId, options) {
    return request("post", `/wifi/${interfaceId}/scan`, {}, options);
  },
  scan_result(interfaceId, options) {
    return request("get", `/wifi/${interfaceId}/scanResult`, {}, options);
  },
  scan_ssid(interfaceId, postData, options) {
    return request("post", `/wifi/${interfaceId}/scanSsid`, postData, options);
  },
  scan_result_ssid(interfaceId, postData, options) {
    return request(
      "put",
      `/wifi/${interfaceId}/scanResultSsid`,
      postData,
      options
    );
  }
};

export default {
  radio,
  wps,
  ssid,
  network,
  restart,
  scan
};
