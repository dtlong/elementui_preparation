/* eslint-disable */
let CryptoJS = require("crypto-js");
const PREFIX = "hmx#cpe@pbkdf2*SALT!";
const keySize = 512 / 32;
/**
 * keySize Note
 *  * 128 bits: 128 / 32
 *  * 256 bits: 256 / 32
 *  * 512 bits: 512 / 32
 */
export default {
  /**
   * @function getPasswordHash
   * @param password
   */
  getPasswordHash(id, password) {
    let HMXSALT = PREFIX + id;
    let salt = window.btoa(HMXSALT);
    let hash = CryptoJS.PBKDF2(password, salt, {
      keySize: keySize,
      iterations: 1000,
      hasher: CryptoJS.algo.SHA512
    });
    return hash.toString(CryptoJS.enc.Base64);
  }
};
