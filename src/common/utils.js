import Vue from "vue";
import Cookie from "vue-cookie";

Vue.use(Cookie);

const isArray = arr => {
  return Array.isArray(arr) || arr instanceof Array;
};
const isDefined = value => {
  return typeof value !== "undefined";
};
const parsePassword = val => {
  let ret = "";
  let temps = val.split(":");
  if (temps.length < 3) {
    throw "Invalid format. greater than 2";
  } else if (temps.length === 3) {
    ret = temps[2];
  } else if (temps.length > 3) {
    temps.splice(0, 2);
    ret = temps.join(":");
  }
  return ret;
};

const equals = (o1, o2) => {
  if (o1 === o2) return true;
  if (o1 === null || o2 === null) return false;
  if (o1 !== o1 && o2 !== o2) return true; // NaN === NaN
  let t1 = typeof o1,
    t2 = typeof o2,
    length = 0,
    key = null,
    keySet = null;
  if (t1 === t2 && t1 === "object") {
    if (isArray(o1)) {
      if (!isArray(o2)) return false;
      if ((length = o1.length) === o2.length) {
        for (key = 0; key < length; key++) {
          if (!equals(o1[key], o2[key])) return false;
        }
        return true;
      }
    } else {
      if (isArray(o2)) return false;
      keySet = Object.create(null);
      for (key in o1) {
        if (!equals(o1[key], o2[key])) return false;
        keySet[key] = true;
      }
      for (key in o2) {
        if (!(key in keySet) && isDefined(o2[key])) return false;
      }
      return true;
    }
  }
  return false;
};

const createDate = dateString => {
  let ret = new Date(dateString);
  if (ret.toString() !== "Invalid Date") {
    return ret;
  }

  const patt = /[:]|[ ]|[.]/g;
  let dateArr = dateString
    .replace(patt, "-")
    .split("-")
    .map((value, index) => {
      if (index === 1) {
        return parseInt(value) - 1;
      }
      return parseInt(value);
    });
  return new Date(
    dateArr[0],
    dateArr[1],
    dateArr[2],
    dateArr[3],
    dateArr[4],
    dateArr[5]
  );
};

export default {
  encodedString(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  },

  decodedString(str) {
    if (!str || str === "") return "";
    return parsePassword(window.atob(str));
  },

  logicStringPassword(userName, password, prefix) {
    const _prefix = prefix || "HS:";
    return _prefix + userName + ":" + password;
  },

  setAuthInHeader(accessToken) {
    Vue.axios.defaults.headers.common["Access-Token"] = accessToken
      ? `${accessToken}`
      : null;
  },

  copyObject(obj) {
    let output, v, key;
    output = Array.isArray(obj) ? [] : {};

    for (key in obj) {
      v = obj[key];
      if (v) {
        output[key] = typeof v === "object" ? this.copyObject(v) : v;
      } else {
        output[key] = v;
      }
    }
    return output;
  },
  getCookie(id) {
    return Vue.cookie.get(id);
  },

  setCookie(id, value, opt) {
    Vue.cookie.set(id, value, opt);
  },

  deleteCookie(id) {
    Vue.cookie.delete(id);
  },

  compareObjects(o1, o2) {
    return equals(o1, o2);
  },
  randomString(len, charSet) {
    charSet =
      charSet ||
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let randomString = "";
    for (let i = 0; i < len; i++) {
      const randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  },
  checkFirstOctet(ip) {
    const firstOctet = ip.split(".")[0];
    switch (firstOctet) {
      case "192":
        return 3;
      case "172":
        return 2;
      case "10":
        return 1;
    }
  },
  convertNumber(data, defaultValue) {
    defaultValue = !isNaN(defaultValue) ? defaultValue : null;
    const number = parseInt(data);
    return isNaN(number) ? defaultValue : number;
  },
  convertString(data, defaultValue) {
    if (typeof data === "string") {
      return data;
    }

    let string = defaultValue || "";
    if (typeof data === "number" || typeof data === "boolean") {
      string = data.toString();
    }
    return string;
  },
  preventInputKeydown(e) {
    /**
     * Allow: backspace, delete, tab, escape, enter
     */

    if (
      [8, 9, 13, 27, 46].indexOf(e.keyCode) !== -1 ||
      /**
       * Allow: Ctrl+A, Command+A, Ctrl+C, Command+C, Ctrl+R, Command+R, Ctrl+V, Command+V, Ctrl+X, Command+X
       */
      ((e.keyCode == 65 ||
        e.keyCode == 67 ||
        e.keyCode == 82 ||
        e.keyCode == 86 ||
        e.keyCode == 88) &&
        (e.ctrlKey === true || e.metaKey === true)) ||
      /**
       * Allow: home, end, left, right, down, up and allow when text is highlighted
       */
      (e.keyCode >= 35 && e.keyCode <= 40) ||
      (e.keyCode >= 112 && e.keyCode <= 123)
    ) {
      return;
    } else {
      e.preventDefault();
    }
  },

  preventNonNumeric(e) {
    /**
     * List keycode:
     * 'backspace' - 8, 'tab' - 9, 'enter' - 13, 'escape' - 27, 'home' - 35, 'end' - 36, 'delete' - 46
     * 'left' - 37, 'up' - 38, 'right' - 39, 'down' - 40
     * '0-9' - 48 - 57 and 96 -105
     * 'a' - 65, 'c' - 67, 'v' - 86, 'x' - 88
     * '.' - 110 (decimal point) and 190 (period)
     */
    /**
     * Allow: backspace, delete, tab, escape, enter
     */
    if (
      [8, 9, 13, 27, 46].indexOf(e.keyCode) !== -1 ||
      /**
       * Allow: Ctrl+A, Command+A, Ctrl+C, Command+C, Ctrl+R, Command+R, Ctrl+V, Command+V, Ctrl+X, Command+X
       */
      ((e.keyCode == 65 ||
        e.keyCode == 67 ||
        e.keyCode == 82 ||
        e.keyCode == 86 ||
        e.keyCode == 88) &&
        (e.ctrlKey === true || e.metaKey === true)) ||
      /**
       * Allow: home, end, left, right, down, up and allow when text is hightlighted
       */
      (e.keyCode >= 35 && e.keyCode <= 40) ||
      (e.keyCode >= 112 && e.keyCode <= 123)
    ) {
      return;
    }
    /**
     * Ensure that it is a number and stop the keypress
     */
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  },
  maskMacAddress(value) {
    if (value === undefined || value === null) {
      return;
    }
    let macAddress = value || "";
    if (macAddress !== "") {
      let deviceMac = macAddress;
      deviceMac = deviceMac.toUpperCase();
      if (deviceMac.length >= 3 && deviceMac.length <= 16) {
        deviceMac = deviceMac.replace(/\W/gi, "");
        deviceMac = deviceMac.replace(/(.{2})/g, "$1:");
      }
      return deviceMac;
    }
    return value;
  },
  checkIsChangeData(listData, rule) {
    if (listData.length === null || listData.length === undefined) {
      return false;
    }
    let obj = listData.filter(e => {
      return equals(rule, e);
    });
    return obj.length === 1 ? false : true;
  },
  parseArrayForSelectBox(array, setIndex) {
    if (!Array.isArray(array) || !array) {
      return [];
    } else {
      let tempArr = [];
      for (let i = 0, len = array.length; i < len; i++) {
        let tempObj = {};
        if (setIndex) {
          tempObj.value = i;
        } else {
          tempObj.value = array[i];
        }
        tempObj.text = array[i].text;
        tempObj.dst = array[i].dst;
        tempArr.push(tempObj);
      }
      return tempArr;
    }
  },
  checkSecurityLevel(input) {
    if (input === null || input === undefined || input === "") {
      return;
    }
    const pattern = {
      sameChar: new RegExp(/^(.)\1+$/),
      textOnly: new RegExp(/^[a-zA-Z]+$/),
      numberOnly: new RegExp(/^[0-9]+$/),
      numAndChar: new RegExp(/^[a-zA-Z0-9]+$/),
      numAndUpper: new RegExp(/^[A-Z0-9]+$/),
      numAndLower: new RegExp(/^[a-z0-9]+$/)
    };
    let level = null;
    const length = input.length;
    const minimumLengthWeak = 4;
    const minimumLengthMedium = 7;
    if (length === 0) {
      level = null;
    } else {
      level = 3;

      if (
        pattern.sameChar.test(input) ||
        pattern.numberOnly.test(input) ||
        pattern.textOnly.test(input) ||
        (pattern.numAndUpper.test(input) && length < minimumLengthWeak) ||
        (pattern.numAndLower.test(input) && length < minimumLengthWeak)
      ) {
        level = 1;
      } else if (
        length < minimumLengthMedium ||
        (pattern.numAndUpper.test(input) && length >= minimumLengthWeak) ||
        (pattern.numAndLower.test(input) && length >= minimumLengthWeak)
      ) {
        level = 2;
      }
    }

    return level;
  },
  formatSpace(mbSpace, decimals) {
    let obj = {
      number: 0,
      unit: "usb_text_MB"
    };

    if (typeof mbSpace === "string") {
      mbSpace = this.convertNumber(mbSpace);
    }

    if (typeof decimals === "string") {
      decimals = this.convertNumber(decimals);
    }

    if (typeof mbSpace === "undefined" || mbSpace === null || mbSpace === 0) {
      return obj;
    }

    const k = 1024;
    const sizes = ["usb_text_MB", "usb_text_GB", "usb_text_TB"];
    const dm =
      typeof decimals === "undefined" || decimals === null ? 1 : decimals;
    const i = Math.floor(Math.log(mbSpace) / Math.log(k));

    obj.number = parseFloat((mbSpace / Math.pow(k, i)).toFixed(dm));
    obj.unit = sizes[i];

    return obj;
  },
  calculateUsedSize(available, total) {
    if (typeof available === "string") {
      available = this.convertNumber(total);
    }
    if (typeof total === "string") {
      total = this.convertNumber(total);
    }
    let obj = {
      used: 0,
      usedPercent: ""
    };
    obj.used = total - available;
    obj.usedPercent = Math.round((obj.used / total) * 100);
    return obj;
  },
  formatDaysAgo(time, inBaseTime) {
    let obj = {
      displayDatetime: "",
      number: 0,
      text: ""
    };
    switch (typeof time) {
      case "number":
      case "string":
        time = createDate(time);
        break;
      case "object":
        break;
      case "undefined":
        return obj;
      default:
        time = new Date();
    }

    if (isNaN(time.getTime())) {
      return obj;
    }

    let baseTime;
    switch (typeof inBaseTime) {
      case "number":
      case "string":
        baseTime = createDate(inBaseTime);
        break;
      case "object":
        break;
      case "undefined":
      default:
        baseTime = new Date();
    }

    const msPerMinute = 60,
      msPerHour = msPerMinute * 60,
      msPerDay = msPerHour * 24,
      elapsed = (+baseTime.getTime() - time.getTime()) / 1000;

    let months = time.getMonth() + 1;
    months = months > 9 ? months : "0" + months;
    let date = time.getDate();
    date = date > 9 ? date : "0" + date;

    if (elapsed < msPerDay) {
      let hours = Math.round(elapsed / msPerHour);
      obj.number = hours;
      if (hours === 1 || hours === 0) {
        obj.text = "disconnected_devices_homepage_one_hour_ago";
      } else if (hours >= 24) {
        obj.number = 1;
        obj.text = "disconnected_devices_homepage_one_day_ago";
      } else {
        obj.text = "disconnected_devices_homepage_many_hours_ago";
      }
    } else {
      const days = Math.round(elapsed / msPerDay);
      obj.number = days;

      if (days === 1) {
        obj.text = "disconnected_devices_homepage_one_day_ago";
      } else if (days >= 7) {
        obj.number = 0;
        obj.displayDatetime = time.getFullYear() + "." + months + "." + date;
      } else {
        obj.text = "disconnected_devices_homepage_many_days_ago";
      }
    }
    return obj;
  },
  convertBoolean(data) {
    if (typeof data === "boolean") {
      return data;
    }

    let boolMap = {
      no: false,
      false: false,
      unactive: false,
      "0": false,
      off: false
    };
    let checkBoolValue = bValue => {
      return boolMap.hasOwnProperty(bValue) ? false : true;
    };

    let ret = false;
    if (typeof data === "string") {
      let lowerCase = data.toLowerCase();
      ret = checkBoolValue(lowerCase);
    } else if (typeof data === "number") {
      ret = checkBoolValue(data);
    }

    return ret;
  },
  getTextNameById(id, list) {
    if (id === null || id === undefined) {
      return;
    }
    const vList = Array.from(list ? list : []);
    const vId = vList.findIndex(item => {
      return item.value === id;
    });
    return vList[vId].text;
  },
  checkDisableAddButton(cdata) {
    if (!cdata.hasOwnProperty("rules")) {
      return false;
    } else if (
      cdata.rules !== undefined &&
      cdata.rules.length >= cdata.maxRules
    ) {
      return true;
    } else {
      return false;
    }
  },
  readFile(urlFile) {
    if (!urlFile || urlFile === "" || typeof urlFile !== "string") {
      return;
    }
  },
  exportErrorAPIMsg(code) {
    if (!code || typeof code !== "number") {
      return;
    }
    let msg = "";
    switch(code) {
      case 400:
        msg = "Bad request";
        break;
      case 401:
        msg = "Unauthorized"
        break;
      case 404:
        msg = "API Not found"
        break;
      case 500:
        msg = "Internal Server Error"
        break;
      case 501:
        msg = "Not implemented API"
        break;
      default:
        msg = "Unexpected error"
        break;
    }
    return msg;
  }
};
