import Utils from "@/common/utils";
class IpPortFilteringRuleModel {
  constructor(data) {
    this.active = false;
    this.id = null;
    this.endPort = 0;
    this.startPort = 0;
    this.ipAddress = "";
    this.name = "";
    this.protocol = "";
    //
    this.port = "";
    //
    this.processData(data);
  }
  processData(data) {
    if (!data) {
      return;
    }
    if (data.hasOwnProperty("active")) {
      this.active = data.active;
    } else {
      this.active = false;
    }

    if (data.hasOwnProperty("id")) {
      this.id = data.id;
    } else {
      this.id = null;
    }

    if (data.hasOwnProperty("name")) {
      this.name = data.name;
    } else {
      this.name = "";
    }

    if (data.hasOwnProperty("ipAddress")) {
      this.ipAddress = data.ipAddress;
    } else {
      this.ipAddress = "";
    }

    if (data.hasOwnProperty("endPort")) {
      this.endPort = Utils.convertNumber(data.endPort);
    } else {
      this.endPort = 0;
    }

    if (data.hasOwnProperty("startPort")) {
      this.startPort = Utils.convertNumber(data.startPort);
    } else {
      this.startPort = 0;
    }

    if (data.hasOwnProperty("protocol")) {
      this.protocol = data.protocol;
    } else {
      this.protocol = "";
    }

    if (data.hasOwnProperty("port")) {
      this.port = data.port;
    } else {
      this.port = "";
    }
  }
  exportPostData() {
    let postData = {
      active: this.active,
      endPort: Utils.convertNumber(this.endPort),
      ipAddress: this.ipAddress,
      name: this.name,
      protocol: this.protocol,
      startPort: Utils.convertNumber(this.startPort)
    };
    return postData;
  }
}

export default IpPortFilteringRuleModel;
