import MacfilteringRuleModel from "./MacfilteringRuleModel";
class MacFilteringModel {
  constructor(data) {
    this.maxRules = 32;
    this.rules = [];
    this.processData(data);
  }
  processData(data) {
    if (!data) {
      return;
    }
    if (data.hasOwnProperty(this.maxRules)) {
      this.maxRules = data.maxRules;
    }

    if (data.hasOwnProperty("rules")) {
      data.rules.forEach(e => {
        this.rules.push(new MacfilteringRuleModel(e));
      });
    }
  }
}

export default MacFilteringModel;
