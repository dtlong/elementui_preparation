class MacfilteringRule {
  constructor(data) {
    this.defaultDeviceName = "Unknown device";
    this.active = false;
    this.deviceId = null;
    this.macAddress = "";
    this.name = this.defaultDeviceName;
    this.id = null;
    this.processData(data);
  }
  processData(data) {
    if (!data) {
      return;
    }
    if (data.hasOwnProperty("active")) {
      this.active = data.active;
    } else {
      this.active = false;
    }

    if (data.hasOwnProperty("deviceId")) {
      this.deviceId = data.deviceId;
    } else {
      this.deviceId = null;
    }

    if (data.hasOwnProperty("macAddress")) {
      this.macAddress = data.macAddress;
    } else {
      this.macAddress = this.defaultMACAddress;
    }

    if (data.hasOwnProperty("name")) {
      this.name = data.name;
    } else {
      this.name = this.defaultDeviceName;
    }

    if (data.hasOwnProperty("id")) {
      this.id = data.id;
    } else {
      this.id = this.null;
    }
  }
  exportPostData() {
    let postData = {
      active: this.active,
      deviceId: this.deviceId,
      macAddress: this.macAddress,
      name: this.name
    };
    return postData;
  }
}

export default MacfilteringRule;
