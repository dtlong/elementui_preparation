/* eslint-disable */
//cookie
export const COOKIE_ACCESS_TOKEN = "accessToken";
export const COOKIE_USER_ROLE = "userRole";
export const COOKIE_USER_NAME = "userName";
export const COOKIE_SWITCH_MODE = "switchMode";
export const COOKIE_PERMISSIONS = "permissions";
export const COOKIE_LANGUAGE = "cookieLanguage";
export const COOKIE_STATUS_POLICY = "statusPolicy";
export const COOKIE_OPMODE = "cookieOperationMode";
export const COOKIE_SYSTEM_TIMER_PING = "cookieSystemTimerPing";
export const COOKIE_FIRMWARE = "cookieFirmware";
export const COOKIE_MODEL_NAME = "cookieModelName";

//type
export const REGO_TYPE = "web";

// APP URL

export const APP_URL = {
  LOGIN: {
    url: "/"
  },
  WIZARD: {
    url: "/welcome"
  },
  HOMEPAGE: {
    url: "/home"
  },
  HOMEPAGE_WAN: {
    url: "/home/wan",
    path: "wan"
  },
  HOMEPAGE_WIRELESS: {
    url: "/home/wireless",
    path: "wireless"
  },
  HOMEPAGE_USB: {
    url: "/home/usb",
    path: "usb"
  },
  HOMEPAGE_DEVICE: {
    url: "/home/device",
    path: "device"
  },
  NETWORK: {
    url: "/network"
  },
  NETWORK_INTERNET: {
    url: "/network/internet",
    path: "internet"
  },
  NETWORK_LAN: {
    url: "/network/lan",
    path: "lan"
  },
  NETWORK_OPERATION: {
    url: "/network/operationmode",
    path: "operationmode"
  },
  WIRELESS: {
    url: "/wireless"
  },
  WIRELESS_PRIMARY: {
    url: "/wireless/primarynetwork",
    path: "primarynetwork"
  },
  WIRELESS_GUEST: {
    url: "/wireless/guestnetwork",
    path: "guestnetwork"
  },
  WIRELESS_WPS: {
    url: "/wireless/wps",
    path: "wps"
  },
  WIRELESS_REPEATER: {
    url: "/wireless/repeater",
    path: "repeater"
  },
  MEDIASHARE: {
    url: "/mediashare"
  },
  MEDIASHARE_SERVERSETTING: {
    url: "/mediashare/serversetting",
    path: "serversetting"
  },
  MEDIASHARE_USB: {
    url: "/mediashare/usb",
    path: "usb"
  },
  QOS: {
    url: "/advanced/qos"
  },
  SECURITY: {
    url: "/security"
  },
  SECURITY_PARENTAL_CONTROL: {
    url: "/security/parentalcontrol",
    path: "parentalcontrol"
  },
  SECURITY_FILTERING: {
    url: "/security/filtering",
    path: "filtering"
  },
  SECURITY_FIREWALL: {
    url: "/security/firewall",
    path: "firewall"
  },
  SECURITY_VPN: {
    url: "/security/vpn",
    path: "vpn"
  },
  SECURITY_CHECK: {
    url: "/security/selfcheck",
    path: "selfcheck"
  },
  ADVANCED: {
    url: "/advanced"
  },
  ADVANCED_NETWORK: {
    url: "/advanced/network",
    path: "network"
  },
  ADVANCED_IPTV: {
    url: "/advanced/iptv",
    path: "iptv"
  },
  ADVANCED_DDNS: {
    url: "/advanced/ddnswol",
    path: "ddnswol"
  },
  ADVANCED_WIRELESS: {
    url: "/advanced/wireless",
    path: "wireless"
  },
  ADVANCED_PORT_FORWARDING: {
    url: "/advanced/portforwarding",
    path: "portforwarding"
  },
  ADVANCED_PORT_TRIGGERING: {
    url: "/advanced/porttriggering",
    path: "porttriggering"
  },
  ADVANCED_RIP: {
    url: "/advanced/rip",
    path: "rip"
  },
  ADVANCED_UPNP: {
    url: "/advanced/upnp",
    path: "upnp"
  },
  ADVANCED_IPV6: {
    url: "/advanced/ipv6",
    path: "ipv6"
  },
  ADVANCED_DIAGNOSTICS: {
    url: "/advanced/diagnostics",
    path: "diagnostics"
  },
  ADVANCED_LOG: {
    url: "/advanced/logs",
    path: "logs"
  },
  NOT_FOUND: {
    url: "*"
  }
};

/* System menu*/
export const SYSTEM_MENU = {
  WAN: "wan",
  WAN_SECONDARY: "wanSecondary",
  WIRELESS_24G: "wireless24g",
  WIRELESS_5G: "wireless5g",
  HELP_GUIDE: "helpGuide",
  SYSTEM: "system",
  LOGOUT: "logout"
};

export const SYSTEM_SUB_MENU = {
  SUB_MENU_LANGUAGE: "language",
  SUB_MENU_CHANGE_PASSWORD: "changePassword",
  SUB_MENU_DATE_TIME: "dateTime",
  SUB_MENU_FIRMWARE_UPDATE: "firmwareUpdate",
  SUB_MENU_POWER_SAVING_MODE: "powerSavingMode",
  SUB_MENU_BACKUP_RESTORE: "backupRestore",
  SUB_MENU_FACTORY_RESTART: "factoryRestart",
  SUB_MENU_LED_MODE: "ledMode"
};

export const SECURITY_TYPE = {
  NONE: "NONE",
  WPA2_PSK: "WPA2-PSK",
  WPA_PSK: "WPA-PSK",
  WPA2_WPA_PSK: "WPA2/WPA-PSK",
  WPA2_ENTERPRISE: "WPA2-Enterprise",
  WPA2_WPA_ENTERPRISE: "WPA2/WPA-Enterprise",
  WEP: "WEP"
};

export const LIST_CONNECTION_STATE = [
  {
    text: "cable_modem_not_ready",
    value: 0
  },
  {
    text: "cable_modem_not_synchronized",
    value: 1
  },
  {
    text: "cable_modem_phy_synchronized",
    value: 2
  },
  {
    text: "cable_modem_us_param_acquired",
    value: 3
  },
  {
    text: "cable_modem_ranging_complete",
    value: 4
  },
  {
    text: "cable_modem_ip_complete",
    value: 5
  },
  {
    text: "cable_modem_tod_established",
    value: 6
  },
  {
    text: "cable_modem_security_established",
    value: 7
  },
  {
    text: "cable_modem_param_transfer_complete",
    value: 8
  },
  {
    text: "cable_modem_registration_complete",
    value: 9
  },
  {
    text: "cable_modem_operational",
    value: 10
  },
  {
    text: "cable_modem_access_denied",
    value: 11
  },
  {
    text: "cable_modem_other",
    value: 12
  }
];

export const LIST_BPI_STATE = [
  {
    text: "cable_modem_disabled",
    value: 0
  },
  {
    text: "cable_modem_bpi",
    value: 1
  },
  {
    text: "cable_modem_bpi_plus",
    value: 2
  },
  {
    text: "cable_modem_failed",
    value: 3
  }
];

export const LIST_CONNECTION_TYPE = [
  {
    text: "network_internet_page_connection_type_dynamic_title",
    value: "dynamic"
  },
  {
    text: "network_internet_page_connection_type_static_title",
    value: "static"
  },
  {
    text: "network_internet_page_card_pppoe",
    value: "pppoe"
  }
];

/* */
export const IPV4 = {
  SUBNET_MASKS: ["255.255.255.0", "255.255.0.0", "255.0.0.0"],
  CLASS_IPVs: ["192", "172", "10"],
  SUB_CLASS_IPs: [
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
    "24",
    "25",
    "26",
    "27",
    "28",
    "29",
    "30",
    "31"
  ]
};

/* */
export const LIST_TCP = {
  BOTH: "TCP/UDP",
  TCP: "TCP",
  UDP: "UDP"
};
//temp string id
//TODO : change string
export const LIST_MODE = [
  {
    text: "advanced_network_page_card_switch_option_Mode_value_router",
    value: "router"
  },
  {
    text: "advanced_network_page_card_switch_option_Mode_value_bridge",
    value: "bridge"
  }
];
export const SERVICE_TYPE = {
  HTTP: {
    NAME: "HTTP",
    START_PORT: 80,
    END_PORT: 80,
    PROTOCOL: "TCP"
  },
  HTTPS: {
    NAME: "HTTPS",
    START_PORT: 443,
    END_PORT: 443,
    PROTOCOL: "TCP"
  },
  FTP: {
    NAME: "FTP",
    START_PORT: 20,
    END_PORT: 21,
    PROTOCOL: "TCP"
  },
  TFTP: {
    NAME: "TFTP",
    START_PORT: 69,
    END_PORT: 69,
    PROTOCOL: "UDP"
  },
  SMTP: {
    NAME: "SMTP",
    START_PORT: 25,
    END_PORT: 25,
    PROTOCOL: "TCP"
  },
  POP3: {
    NAME: "POP3",
    START_PORT: 110,
    END_PORT: 110,
    PROTOCOL: "TCP"
  },
  NNTP: {
    NAME: "NNTP",
    START_PORT: 119,
    END_PORT: 119,
    PROTOCOL: "TCP"
  },
  TELNET: {
    NAME: "Telnet",
    START_PORT: 23,
    END_PORT: 23,
    PROTOCOL: "TCP"
  },
  IRC: {
    NAME: "IRC",
    START_PORT: 194,
    END_PORT: 194,
    PROTOCOL: "TCP"
  },
  SNMP: {
    NAME: "SNMP",
    START_PORT: 161,
    END_PORT: 162,
    PROTOCOL: "UDP"
  },
  FINGER: {
    NAME: "Finger",
    START_PORT: 79,
    END_PORT: 79,
    PROTOCOL: "TCP"
  },
  GOPHER: {
    NAME: "Gopher",
    START_PORT: 70,
    END_PORT: 70,
    PROTOCOL: "TCP"
  },
  LDAP: {
    NAME: "LDAP",
    START_PORT: 389,
    END_PORT: 389,
    PROTOCOL: "TCP"
  },
  UUCP: {
    NAME: "UUCP",
    START_PORT: 540,
    END_PORT: 540,
    PROTOCOL: "TCP"
  },
  RDP: {
    NAME: "RDP",
    START_PORT: 3389,
    END_PORT: 3389,
    PROTOCOL: "TCP"
  }
};

export const SERVER_OPTIONS = [
  {
    serviceType: SERVICE_TYPE.HTTP.NAME,
    start_port: SERVICE_TYPE.HTTP.START_PORT,
    end_port: SERVICE_TYPE.HTTP.END_PORT,
    protocol: SERVICE_TYPE.HTTP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.HTTPS.NAME,
    start_port: SERVICE_TYPE.HTTPS.START_PORT,
    end_port: SERVICE_TYPE.HTTPS.END_PORT,
    protocol: SERVICE_TYPE.HTTPS.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.FTP.NAME,
    start_port: SERVICE_TYPE.FTP.START_PORT,
    end_port: SERVICE_TYPE.FTP.END_PORT,
    protocol: SERVICE_TYPE.FTP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.TFTP.NAME,
    start_port: SERVICE_TYPE.TFTP.START_PORT,
    end_port: SERVICE_TYPE.TFTP.END_PORT,
    protocol: SERVICE_TYPE.TFTP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.SMTP.NAME,
    start_port: SERVICE_TYPE.SMTP.START_PORT,
    end_port: SERVICE_TYPE.SMTP.END_PORT,
    protocol: SERVICE_TYPE.SMTP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.POP3.NAME,
    start_port: SERVICE_TYPE.POP3.START_PORT,
    end_port: SERVICE_TYPE.POP3.END_PORT,
    protocol: SERVICE_TYPE.POP3.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.NNTP.NAME,
    start_port: SERVICE_TYPE.NNTP.START_PORT,
    end_port: SERVICE_TYPE.NNTP.END_PORT,
    protocol: SERVICE_TYPE.NNTP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.TELNET.NAME,
    start_port: SERVICE_TYPE.TELNET.START_PORT,
    end_port: SERVICE_TYPE.TELNET.END_PORT,
    protocol: SERVICE_TYPE.TELNET.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.IRC.NAME,
    start_port: SERVICE_TYPE.IRC.START_PORT,
    end_port: SERVICE_TYPE.IRC.END_PORT,
    protocol: SERVICE_TYPE.IRC.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.SNMP.NAME,
    start_port: SERVICE_TYPE.SNMP.START_PORT,
    end_port: SERVICE_TYPE.SNMP.END_PORT,
    protocol: SERVICE_TYPE.SNMP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.FINGER.NAME,
    start_port: SERVICE_TYPE.FINGER.START_PORT,
    end_port: SERVICE_TYPE.FINGER.END_PORT,
    protocol: SERVICE_TYPE.FINGER.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.GOPHER.NAME,
    start_port: SERVICE_TYPE.GOPHER.START_PORT,
    end_port: SERVICE_TYPE.GOPHER.END_PORT,
    protocol: SERVICE_TYPE.GOPHER.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.LDAP.NAME,
    start_port: SERVICE_TYPE.LDAP.START_PORT,
    end_port: SERVICE_TYPE.LDAP.END_PORT,
    protocol: SERVICE_TYPE.LDAP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.UUCP.NAME,
    start_port: SERVICE_TYPE.UUCP.START_PORT,
    end_port: SERVICE_TYPE.UUCP.END_PORT,
    protocol: SERVICE_TYPE.UUCP.PROTOCOL
  },
  {
    serviceType: SERVICE_TYPE.RDP.NAME,
    start_port: SERVICE_TYPE.RDP.START_PORT,
    end_port: SERVICE_TYPE.RDP.END_PORT,
    protocol: SERVICE_TYPE.RDP.PROTOCOL
  }
];

export const EVENT_BUS = {
  RESET_DIALOG: "resetDialog",
  OPEN_CONFIRM_DIALOG: "open-confirm-dialog",
  CLOSE_CONFIRM_DIALOG: "close-confirm-dialog",
  OPEN_COMPLETE_DIALOG: "open-complete-dialog",
  CLOSE_COMPLETE_DIALOG: "close-complete-dialog",
  OPEN_ERROR_CASE_DIALOG: "open-error-dialog",
  CLOSE_ERROR_CASE_DIALOG: "close-error-dialog",
  OPEN_RESTART_DIALOG: "open-restart-dialog",
  CLOSE_RESTART_DIALOG: "close-restart-dialog",
  OPEN_FACTORY_DIALOG: "open-factory-dialog",
  CLOSE_FACTORY_DIALOG: "close-factory-dialog",
  CUSTOM_MESSAGE: "custom-message",
  CLOSE_SYSTEM_DIALOG: "closeSystemDialog",
  OPEN_SYSTEM_DIALOG: "openSystemDialog",
  OPEN_SPINNER_DIALOG: "openSpinnerDialog",
  CLOSE_SPINNER_DIALOG: "closeSpinnerDialog",
  OPEN_SPINNER_DIALOG2: "openSpinnerDialog2",
  CLOSE_SPINNER_DIALOG2: "closeSpinnerDialog2",
  SAVE_ACCESS_CONTROL: "saveAccessControl",
  FETCH_SSID: "fetchSSID",
  OPEN_PRIVACY_DIALOG: "open-privacy-dialog",
  CLOSE_PRIVACY_DIALOG: "close-privacy-dialog",
  AGREE_PRIVACY: "agree-privacy",
  CPU_NEW_DATA: "cpuNewData",
  MEMORY_NEW_DATA: "memoryNewData",
  OPEN_CUSTOM_DIALOG: "openCustomDialog",
  RE_FETCH_DATA: "reFetchData",
  SHARE_DATA_ACCESS_LINK: "shareDataAccessLink",
  CALL_REFRESH_CONNECTIVITY: "callRefreshConnectivity",

  // Folder Tree
  SELECT_FOLDER_PATH: "updateFolderPath",
  CLEAR_DIAGNOSTICS_RESULT: "clearDiagnosticsResult",
  SHOW_RESULT_DIAGNOSTICS: "showDiagnosticsResult",

  // CPU-MEMMORY
  STOP_CPU: "stopCPU",
  STOP_MEMORY: "stopMemory",
  RESET_CPU: "resetCPU",
  RESET_MEMORY: "resetMemory",
  //
  SHOW_ERROR_CASE: "showErrorCase",
  RESET_HEADER_ICON_WIRELESS: "resetHeaderIcon",
  // Wizard
  SKIP_WIZARD: "skipWizard",

  RECALCULATE_STYLE: "recalculateStyle",

  WS: {
    USB_INSERTED: "usbInserted",
    USB_REMOVED: "usbRemoved",
    WAN_CONNECTED: "wanConnected",
    WAN_DISCONNECTED: "wanDisconnected"
  },

  UPDATE_DHCP_RANGE: "updateDHCPRange",

  // Parental
  OPEN_CREATE_PASSWORD: "openCreatePassword",
  // Device connectivity
  UPDATE_DEVICE_CONNECTIVITY: "updateDeviceConnectivity",
  SHOW_NOTIFICATION: "showNotification"
};

export const CUSTOM_DIALOG_NAME = {
  DEVICE_INFORMATION_DIALOG: "deviceInformationDialog",
  FOLDER_TREE_DIALOG: "folderTreeDialog"
};

export const CUSTOM_CARD_NAME = {
  NETWORK_FOLDER_CARD: "networkFolderCard"
};

export const WIRELESS_TYPE = {
  TYPE_24: "2.4Ghz",
  TYPE_5: "5Ghz",
  PRIMARY: "primary",
  GUEST: "guest",
  MAX_GUEST_RULES: 7,
  GUEST_SSID_24G: "HUMAX_Guest_2G!",
  GUEST_SSID_5G: "HUMAX_Guest_5G!"
};

export const LIST_SECURITY_TYPE = [
  {
    text: "wireless_homepage_security_none",
    value: "NONE"
  },
  {
    text: "wireless_homepage_security_wpa2_psk",
    value: "WPA2-PSK"
  },
  {
    text: "wireless_homepage_security_wpa2_wpa_psk",
    value: "WPA2/WPA-PSK"
  },
  {
    text: "wireless_homepage_security_wpa2_enterprise",
    value: "WPA2-Enterprise"
  },
  {
    text: "wireless_homepage_security_wpa2_wpa_enterprise",
    value: "WPA2/WPA-Enterprise"
  },
  {
    text: "wireless_homepage_security_wep",
    value: "WEP"
  }
];

export const LIST_ENCRYPTION = [
  {
    text: "wireless_page_encryption_tkip",
    value: "TKIP"
  },
  {
    text: "wireless_page_encryption_aes",
    value: "AES"
  },
  {
    text: "wireless_page_encryption_aes_tkip",
    value: "AES/TKIP"
  },
  {
    text: "wireless_page_encryption_wep64",
    value: "WEP64"
  },
  {
    text: "wireless_page_encryption_wep128",
    value: "WEP128"
  }
];

export const LIST_KEY_TYPE = [
  {
    text: "wireless_page_key_type_character",
    value: "Character String"
  },
  {
    text: "wireless_page_key_type_hexadecimal",
    value: "Hexadecimal"
  }
];

export const TIMEZONE_GMT = [
  {
    text: "(GMT-12:00) Eniwetok, Kwajalein",
    dst: false
  },
  {
    text: "(GMT-11:00) Niue",
    dst: false
  },
  {
    text: "(GMT-11:00) Midway, Pago Pago",
    dst: false
  },
  {
    text: "(GMT-11:00) Apia",
    dst: false
  },
  {
    text: "(GMT-10:00) Rarotonga",
    dst: false
  },
  {
    text: "(GMT-10:00) Adak",
    dst: true
  },
  {
    text: "(GMT-10:00) Honolulu, Johnston",
    dst: false
  },
  {
    text: "(GMT-10:00) Tahiti",
    dst: false
  },
  {
    text: "(GMT-10:00) Fakaofo",
    dst: false
  },
  {
    text: "(GMT-09:30) Marquesas",
    dst: false
  },
  {
    text: "(GMT-09:00) Anchorage, Juneau, Nome, Yakutat",
    dst: true
  },
  {
    text: "(GMT-09:00) Gambier",
    dst: false
  },
  {
    text: "(GMT-08:00) Pitcairn",
    dst: false
  },
  {
    text: "(GMT-08:00) Dawson, Los Angeles, Tijuana, Vancouver, Whitehorse",
    dst: true
  },
  {
    text: "(GMT-08:00) Santa Isabel",
    dst: false
  },
  {
    text: "(GMT-07:00) Dawson Creek, Hermosillo, Phoenix",
    dst: false
  },
  {
    text:
      "(GMT-07:00) Boise, Cambridge Bay, Denver, Edmonton, Inuvik, Ojinaga, Shiprock, Yellowknife",
    dst: true
  },
  {
    text: "(GMT-07:00) Chihuahua, Mazatlan",
    dst: true
  },
  {
    text:
      "(GMT-06:00) Belize, Costa Rica, El Salvador, Guatemala, Managua, Regina, Swift Current, Tegucigalpa",
    dst: false
  },
  {
    text:
      "(GMT-06:00) Chicago, Knox, Tell City, Matamoros, Menominee, Center, New Salem, Rainy River, Rankin Inlet, Winnipeg",
    dst: true
  },
  {
    text: "(GMT-06:00) Cancun, Merida, Mexico City, Monterrey",
    dst: true
  },
  {
    text: "(GMT-06:00) Galapagos",
    dst: false
  },
  {
    text: "(GMT-05:00) Bogota",
    dst: false
  },
  {
    text: "(GMT-05:00) Havana",
    dst: true
  },
  {
    text: "(GMT-05:00) Guayaquil",
    dst: false
  },
  {
    text: "(GMT-05:00) Atikokan, Cayman, Jamaica, Panama, Port-au-Prince",
    dst: false
  },
  {
    text:
      "(GMT-05:00) Detroit, Grand Turk, Indianapolis, Marengo, Petersburg, Vevay, Vincennes, Winamac, Iqaluit, Louisville, Monticello, Montreal, Nassau, New York, Nipigon, Pangnirtung, Thunder Bay, Toronto",
    dst: true
  },
  {
    text: "(GMT-05:00) Lima",
    dst: false
  },
  {
    text: "(GMT-04:30) Caracas",
    dst: false
  },
  {
    text: "(GMT-04:00) Boa Vista, Eirunepe, Manaus, Porto Velho, Rio Branco",
    dst: false
  },
  {
    text: "(GMT-04:00) Campo Grande, Cuiaba",
    dst: true
  },
  {
    text:
      "(GMT-04:00) Anguilla, Antigua, Aruba, Barbados, Blanc-Sablon, Curacao, Dominica, Grenada, Guadeloupe, Marigot, Martinique, Montserrat, Port of Spain, Puerto Rico, Santo Domingo, St Barthelemy, St Kitts, St Lucia, St Thomas, St Vincent, Tortola",
    dst: false
  },
  {
    text: "(GMT-04:00) Glace Bay, Halifax, Moncton, Thule, Bermuda",
    dst: true
  },
  {
    text: "(GMT-04:00) Goose Bay",
    dst: true
  },
  {
    text: "(GMT-04:00) La Paz",
    dst: false
  },
  {
    text: "(GMT-04:00) Stanley",
    dst: false
  },
  {
    text: "(GMT-04:00) Guyana",
    dst: false
  },
  {
    text: "(GMT-04:00) Asuncion",
    dst: true
  },
  {
    text: "(GMT-03:30) St Johns",
    dst: true
  },
  {
    text:
      "(GMT-03:00) Buenos Aires, Catamarca, Cordoba, Jujuy, La Rioja, Mendoza, Rio Gallegos, Salta, San Juan, Tucuman, Ushuaia",
    dst: false
  },
  {
    text:
      "(GMT-03:00) Araguaina, Bahia, Belem, Fortaleza, Maceio, Recife, Santarem",
    dst: false
  },
  {
    text: "(GMT-03:00) Sao Paulo",
    dst: true
  },
  {
    text: "(GMT-03:00) Cayenne",
    dst: false
  },
  {
    text: "(GMT-03:00) Miquelon",
    dst: true
  },
  {
    text: "(GMT-03:00) Rothera",
    dst: false
  },
  {
    text: "(GMT-03:00) Paramaribo",
    dst: false
  },
  {
    text: "(GMT-03:00) Montevideo",
    dst: false
  },
  {
    text: "(GMT-02:00) Noronha",
    dst: false
  },
  {
    text: "(GMT-02:00) South Georgia",
    dst: false
  },
  {
    text: "(GMT-01:00) Azores",
    dst: true
  },
  {
    text: "(GMT-01:00) Cape Verde",
    dst: false
  },
  {
    text: "(GMT-01:00) Scoresbysund",
    dst: true
  },
  {
    text:
      "(GMT+00:00) Abidjan, Accra, Bamako, Banjul, Bissau, Conakry, Dakar, Freetown, Lome, Monrovia, Nouakchott, Ouagadougou, Sao Tome, Danmarkshavn, Reykjavik, St Helena",
    dst: false
  },
  {
    text: "(GMT+00:00) Guernsey, Isle of Man, Jersey, London",
    dst: true
  },
  {
    text: "(GMT+00:00) Dublin",
    dst: true
  },
  {
    text: "(GMT+00:00) Casablanca, El Aaiun",
    dst: false
  },
  {
    text: "(GMT+00:00) Canary, Faroe, Madeira, Lisbon",
    dst: true
  },
  {
    text: "(GMT+01:00) Algiers, Tunis",
    dst: false
  },
  {
    text:
      "(GMT+01:00) Ceuta, Longyearbyen, Amsterdam, Andorra, Belgrade, Berlin, Bratislava, Brussels, Budapest, Copenhagen, Gibraltar, Ljubljana, Luxembourg, Madrid, Malta, Monaco, Oslo, Paris, Podgorica, Prague, Rome, San Marino, Sarajevo, Skopje, Stockholm, Tirane, Vaduz, Vatican, Vienna, Warsaw, Zagreb, Zurich",
    dst: true
  },
  {
    text:
      "(GMT+01:00) Bangui, Brazzaville, Douala, Kinshasa, Lagos, Libreville, Luanda, Malabo, Ndjamena, Niamey, Porto-Novo",
    dst: false
  },
  {
    text: "(GMT+01:00) Windhoek",
    dst: true
  },
  {
    text:
      "(GMT+02:00) Blantyre, Bujumbura, Gaborone, Harare, Kigali, Lubumbashi, Lusaka, Maputo",
    dst: false
  },
  {
    text: "(GMT+02:00) Tripoli",
    dst: false
  },
  {
    text: "(GMT+02:00) Kaliningrad, Minsk",
    dst: false
  },
  {
    text: "(GMT+02:00) Beirut",
    dst: true
  },
  {
    text:
      "(GMT+02:00) Nicosia, Athens, Bucharest, Chisinau, Helsinki, Kiev, Mariehamn, Riga, Simferopol, Sofia, Tallinn, Uzhgorod, Vilnius, Zaporozhye",
    dst: true
  },
  {
    text: "(GMT+02:00) Gaza",
    dst: true
  },
  {
    text: "(GMT+02:00) Damascus",
    dst: true
  },
  {
    text: "(GMT+02:00) Johannesburg, Maseru, Mbabane",
    dst: false
  },
  {
    text: "(GMT+03:00) Istanbul, Aden, Baghdad, Bahrain, Kuwait, Qatar, Riyadh",
    dst: false
  },
  {
    text:
      "(GMT+03:00) Addis Ababa, Asmara, Dar es Salaam, Djibouti, Kampala, Khartoum, Mogadishu, Nairobi, Antananarivo, Comoro, Mayotte",
    dst: false
  },
  {
    text: "(GMT+03:00) Samara",
    dst: false
  },
  {
    text: "(GMT+03:00) Syowa",
    dst: false
  },
  {
    text: "(GMT+03:00) Volgograd",
    dst: false
  },
  {
    text: "(GMT+03:30) Tehran",
    dst: false
  },
  {
    text: "(GMT+04:00) Yerevan",
    dst: false
  },
  {
    text: "(GMT+04:00) Baku",
    dst: false
  },
  {
    text: "(GMT+04:00) Tbilisi",
    dst: false
  },
  {
    text: "(GMT+04:00) Dubai, Muscat",
    dst: false
  },
  {
    text: "(GMT+04:00) Moscow",
    dst: false
  },
  {
    text: "(GMT+04:00) Mauritius",
    dst: false
  },
  {
    text: "(GMT+04:00) Reunion",
    dst: false
  },
  {
    text: "(GMT+04:00) Mahe",
    dst: false
  },
  {
    text: "(GMT+04:30) Kabul",
    dst: false
  },
  {
    text: "(GMT+05:00) Aqtau, Aqtobe",
    dst: false
  },
  {
    text: "(GMT+05:00) Mawson",
    dst: false
  },
  {
    text: "(GMT+05:00) Maldives",
    dst: false
  },
  {
    text: "(GMT+05:00) Oral",
    dst: false
  },
  {
    text: "(GMT+05:00) Karachi",
    dst: false
  },
  {
    text: "(GMT+05:00) Kerguelen",
    dst: false
  },
  {
    text: "(GMT+05:00) Dushanbe",
    dst: false
  },
  {
    text: "(GMT+05:00) Ashgabat",
    dst: false
  },
  {
    text: "(GMT+05:00) Samarkand, Tashkent",
    dst: false
  },
  {
    text: "(GMT+05:00) Yekaterinburg",
    dst: false
  },
  {
    text: "(GMT+05:30) Colombo, Kolkata",
    dst: false
  },
  {
    text: "(GMT+05:45) Kathmandu",
    dst: false
  },
  {
    text: "(GMT+06:00) Almaty",
    dst: false
  },
  {
    text: "(GMT+06:00) Dhaka",
    dst: false
  },
  {
    text: "(GMT+06:00) Thimphu",
    dst: false
  },
  {
    text: "(GMT+06:00) Chagos",
    dst: false
  },
  {
    text: "(GMT+06:00) Bishkek",
    dst: false
  },
  {
    text: "(GMT+06:00) Novokuznetsk, Novosibirsk",
    dst: false
  },
  {
    text: "(GMT+06:00) Qyzylorda",
    dst: false
  },
  {
    text: "(GMT+06:00) Vostok",
    dst: false
  },
  {
    text: "(GMT+06:30) Cocos",
    dst: false
  },
  {
    text: "(GMT+06:30) Rangoon",
    dst: false
  },
  {
    text: "(GMT+07:00) Christmas",
    dst: false
  },
  {
    text: "(GMT+07:00) Davis",
    dst: false
  },
  {
    text: "(GMT+07:00) Hovd",
    dst: false
  },
  {
    text: "(GMT+07:00) Bangkok, Ho Chi Minh, Phnom Penh, Vientiane",
    dst: false
  },
  {
    text: "(GMT+07:00) Krasnoyarsk",
    dst: false
  },
  {
    text: "(GMT+07:00) Omsk",
    dst: false
  },
  {
    text: "(GMT+07:00) Jakarta, Pontianak",
    dst: false
  },
  {
    text: "(GMT+08:00) Brunei",
    dst: false
  },
  {
    text: "(GMT+08:00) Choibalsan",
    dst: false
  },
  {
    text: "(GMT+08:00) Makassar",
    dst: false
  },
  {
    text:
      "(GMT+08:00) Chongqing, Harbin, Kashgar, Macau, Shanghai, Taipei, Urumqi",
    dst: false
  },
  {
    text: "(GMT+08:00) Hong Kong",
    dst: false
  },
  {
    text: "(GMT+08:00) Irkutsk",
    dst: false
  },
  {
    text: "(GMT+08:00) Kuala Lumpur, Kuching",
    dst: false
  },
  {
    text: "(GMT+08:00) Manila",
    dst: false
  },
  {
    text: "(GMT+08:00) Singapore",
    dst: false
  },
  {
    text: "(GMT+08:00) Ulaanbaatar",
    dst: false
  },
  {
    text: "(GMT+08:00) Casey, Perth",
    dst: false
  },
  {
    text: "(GMT+08:45) Eucla",
    dst: false
  },
  {
    text: "(GMT+09:00) Jayapura",
    dst: false
  },
  {
    text: "(GMT+09:00) Tokyo",
    dst: false
  },
  {
    text: "(GMT+09:00) Seoul",
    dst: false
  },
  {
    text: "(GMT+09:00) Palau",
    dst: false
  },
  {
    text: "(GMT+09:00) Dili",
    dst: false
  },
  {
    text: "(GMT+09:00) Yakutsk",
    dst: false
  },
  {
    text: "(GMT+09:30) Darwin",
    dst: false
  },
  {
    text: "(GMT+09:30) Adelaide, Broken Hill",
    dst: true
  },
  {
    text: "(GMT+10:00) Guam, Saipan",
    dst: false
  },
  {
    text: "(GMT+10:00) DumontDUrville",
    dst: false
  },
  {
    text: "(GMT+10:00) Brisbane, Lindeman",
    dst: false
  },
  {
    text: "(GMT+10:00) Currie, Hobart, Melbourne, Sydney",
    dst: true
  },
  {
    text: "(GMT+10:00) Port Moresby",
    dst: false
  },
  {
    text: "(GMT+10:00) Sakhalin",
    dst: false
  },
  {
    text: "(GMT+10:00) Truk",
    dst: false
  },
  {
    text: "(GMT+10:00) Vladivostok",
    dst: false
  },
  {
    text: "(GMT+10:30) Lord Howe",
    dst: true
  },
  {
    text: "(GMT+11:00) Anadyr",
    dst: false
  },
  {
    text: "(GMT+11:00) Kosrae",
    dst: false
  },
  {
    text: "(GMT+11:00) Magadan",
    dst: false
  },
  {
    text: "(GMT+11:00) Macquarie",
    dst: false
  },
  {
    text: "(GMT+11:00) Noumea",
    dst: false
  },
  {
    text: "(GMT+11:00) Kamchatka",
    dst: true
  },
  {
    text: "(GMT+11:00) Ponape",
    dst: false
  },
  {
    text: "(GMT+11:00) Guadalcanal",
    dst: false
  },
  {
    text: "(GMT+11:00) Efate",
    dst: false
  },
  {
    text: "(GMT+11:30) Norfolk",
    dst: false
  },
  {
    text: "(GMT+12:00) Fiji",
    dst: false
  },
  {
    text: "(GMT+12:00) Tarawa",
    dst: false
  },
  {
    text: "(GMT+12:00) Kwajalein, Majuro",
    dst: false
  },
  {
    text: "(GMT+12:00) Nauru",
    dst: false
  },
  {
    text: "(GMT+12:00) McMurdo, South Pole, Auckland",
    dst: true
  },
  {
    text: "(GMT+12:00) Funafuti",
    dst: false
  },
  {
    text: "(GMT+12:00) Wake",
    dst: false
  },
  {
    text: "(GMT+12:00) Wallis",
    dst: false
  },
  {
    text: "(GMT+12:45) Chatham",
    dst: true
  },
  {
    text: "(GMT+13:00) Enderbury",
    dst: false
  },
  {
    text: "(GMT+13:00) Tongatapu",
    dst: false
  },
  {
    text: "(GMT+14:00) Kiritimati",
    dst: false
  }
];

export const DATE_TIME_FORMAT = {
  FORMAT: {
    BE: "YYYY/MM/DD HH:mm:ss",
    FE: "YYYY.MM.DD HH:mm:ss"
  },
  SERVER_ARRAY: ["0.pool.ntp.org", "1.pool.ntp.org", "2.pool.ntp.org"],
  MAX_RULE_NTP: 3
};
export const LIST_80211_MODE = [
  {
    text: "802.11b",
    value: "802.11b"
  },
  {
    text: "802.11b+g",
    value: "802.11b+g"
  },
  {
    text: "802.11b+g+n",
    value: "802.11b+g+n"
  }
];

export const LIST_80211_MODE_FIVE = [
  {
    text: "802.11a",
    value: "802.11a"
  },
  {
    text: "802.11a+n",
    value: "802.11a+n"
  },
  {
    text: "802.11a+n+ac",
    value: "802.11a+n+ac"
  }
];
export const LIST_BANDWIDTH_24 = [
  {
    text: "Auto",
    value: "auto"
  },
  {
    text: "20 Mhz",
    value: "20"
  },
  {
    text: "40 Mhz",
    value: "40"
  }
];
export const LIST_BANDWIDTH_5 = [
  {
    text: "Auto",
    value: "auto"
  },
  {
    text: "20 Mhz",
    value: "20"
  },
  {
    text: "40 Mhz",
    value: "40"
  },
  {
    text: "80 Mhz",
    value: "80"
  }
];
export const LIST_SIDEBAND = [
  {
    text: "advanced_wireless_list_sideband_upper",
    value: "upper"
  },
  {
    text: "advanced_wireless_list_sideband_lower",
    value: "lower"
  }
];
export const LIST_OUTPUT_POWER = [
  {
    text: "advanced_wireless_list_output_power_high",
    value: "high"
  },
  {
    text: "advanced_wireless_list_output_power_medium",
    value: "medium"
  },
  {
    text: "advanced_wireless_list_output_power_low",
    value: "low"
  }
];

export const TYPE_ACCOUNT = {
  MEDIASHARE: "mediashare",
  VPN: "vpn",
  WEB: "web"
};

export const USER_PW_SECURITYLEVEL = {
  WEAK: "weak",
  MEDIUM: "medium",
  STRONG: "strong"
};

export const CONNECTION_STATUS = {
  CONNECTED: "connected",
  DISCONNECTED: "disconnected",
  CONNECTING: "connecting"
};

export const WAN_MODE = {
  SINGLE: "single",
  DUAL: "dual"
};

export const WAN_CONNECTION_TYPE = [
  {
    text: "network_internet_page_card_dynamic_ip",
    value: "dynamic"
  },
  {
    text: "network_internet_page_card_static_ip",
    value: "static"
  },
  {
    text: "network_internet_page_card_pppoe",
    value: "pppoe"
  }
];

export const QMODE_OPERATION = {
  ROUTER: "router",
  REPEATER: "repeater",
  AP: "ap",
  MESH_MASTER: "mesh master",
  MESH_SLAVE: "mesh slave",
  MESH_AP: "mesh ap"
};

export const QMODE_TYPE = {
  ROUTER: "router",
  EXTENDER: "extender"
};

export const DEVICE_INTERFACE = {
  LAN: "lan",
  TWO_FOUR: "2.4g",
  FIVE: "5g",
  GUEST_TWO_FOUR: "guest2.4g",
  GUEST_FIVE: "guest5g"
};

export const DEVICE_NETWORK_TYPE = {
  PRIMARY: "primary",
  GUEST: "guest"
};
export const LIST_WAN_NAME = [
  {
    text: "network_internet_page_status_card_ethernet",
    value: "default"
  },
  {
    text: "network_internet_page_status_card_USB",
    value: "UsbMobileModem"
  },
  {
    text: "network_internet_page_status_card_android_tethering",
    value: "AndroidTethering"
  }
];

export const LIST_WAN_TYPE = [
  {
    text: "common_text_load_balance",
    value: 0
  },
  {
    text: "common_text_fail_over",
    value: 1
  }
];

export const LIST_SENSITIVE_ID = ["admin", "Admin", "root", "Anonymous"];

export const NETWORK_FOLDER_PERMISSION = {
  READ_WRITE: "rw",
  READ_ONLY: "ro"
};

export const ANONYMOUS_ACCOUNT = {
  value: 0,
  text: "Anonymous"
};

export const DDNS_TYPE = {
  DYNDNS: 0,
  NOIP: 1,
  DUCKDNS: 2,
  HUMAXDNS: 3
};

export const PROTECTION_LEVEL = {
  mediumLevel: [
    {
      protocol: "AIM/ICQ",
      startEndPort: "5190 - 5190",
      type: "TCP"
    },
    {
      protocol: "DHCPv6",
      startEndPort: "546 - 547",
      type: "UDP"
    },
    {
      protocol: "DNS TCP",
      startEndPort: "53 - 53",
      type: "TCP"
    },
    {
      protocol: "DNS UDP",
      startEndPort: "53 - 53",
      type: "UDP"
    },
    {
      protocol: "FTP-S",
      startEndPort: "989 - 990",
      type: "TCP"
    },
    {
      protocol: "HTTP",
      startEndPort: "80 - 80",
      type: "TCP"
    },
    {
      protocol: "HTTP ALT",
      startEndPort: "8080 - 8080",
      type: "TCP"
    },
    {
      protocol: "HTTP MONGOOSE",
      startEndPort: "8082 - 8082",
      type: "TCP"
    },
    {
      protocol: "HTTP PROXY",
      startEndPort: "8083 - 8083",
      type: "TCP"
    },
    {
      protocol: "HTTP-S",
      startEndPort: "443 - 443",
      type: "TCP"
    },
    {
      protocol: "IMAP",
      startEndPort: "143 - 143",
      type: "TCP"
    },
    {
      protocol: "IMAP-S",
      startEndPort: "993 - 993",
      type: "TCP"
    },
    {
      protocol: "IPSec NAT-T",
      startEndPort: "4500 - 4500",
      type: "UDP"
    },
    {
      protocol: "NTP",
      startEndPort: "123 - 123",
      type: "UDP"
    },
    {
      protocol: "POP3",
      startEndPort: "110 - 110",
      type: "TCP"
    },
    {
      protocol: "POP3-S",
      startEndPort: "995 - 995",
      type: "TCP"
    },
    {
      protocol: "RADIUS",
      startEndPort: "1812 - 1812",
      type: "TCP"
    },
    {
      protocol: "RADIUS",
      startEndPort: "1812 - 1812",
      type: "UDP"
    },
    {
      protocol: "SMTP",
      startEndPort: "25 - 25",
      type: "TCP"
    },
    {
      protocol: "SSH",
      startEndPort: "22 - 22",
      type: "TCP"
    },
    {
      protocol: "SMTP-S",
      startEndPort: "465 - 465",
      type: "TCP"
    },
    {
      protocol: "Steam",
      startEndPort: "1725 - 1725",
      type: "UDP"
    },
    {
      protocol: "Steam Friends",
      startEndPort: "1200 - 1200",
      type: "UDP"
    },
    {
      protocol: "Telnet-S",
      startEndPort: "992 - 992",
      type: "TCP"
    },
    {
      protocol: "XBOX Live",
      startEndPort: "3074 - 3074",
      type: "TCP"
    },
    {
      protocol: "XBOX Live",
      startEndPort: "3074 - 3074",
      type: "UDP"
    },
    {
      protocol: "World of Warcraft",
      startEndPort: "3724 - 3724",
      type: "TCP"
    },
    {
      protocol: "World of Warcraft",
      startEndPort: "3724 - 3724",
      type: "UDP"
    },
    {
      protocol: "Yahoo Messenger",
      startEndPort: "5050 - 5050",
      type: "TCP"
    }
  ],
  highLevel: [
    {
      protocol: "DNS TCP",
      startEndPort: "53 - 53",
      type: "TCP"
    },
    {
      protocol: "DNS UDP",
      startEndPort: "53 - 53",
      type: "UDP"
    },
    {
      protocol: "HTTP",
      startEndPort: "80 - 80",
      type: "TCP"
    },
    {
      protocol: "HTTP-S",
      startEndPort: "443 - 443",
      type: "TCP"
    },
    {
      protocol: "IMAP-S",
      startEndPort: "993 - 993",
      type: "TCP"
    },
    {
      protocol: "IPSec NAT-T",
      startEndPort: "4500 - 4500",
      type: "UDP"
    },
    {
      protocol: "NTP",
      startEndPort: "123 - 123",
      type: "UDP"
    },
    {
      protocol: "POP3-S",
      startEndPort: "995 - 995",
      type: "TCP"
    },
    {
      protocol: "SSH",
      startEndPort: "22 - 22",
      type: "TCP"
    },
    {
      protocol: "SMTP",
      startEndPort: "25 - 25",
      type: "TCP"
    },
    {
      protocol: "SMTP-S",
      startEndPort: "465 - 465",
      type: "TCP"
    }
  ]
};

export const STATUS_CODE = {
  OK: 200,
  NOT_IMPLEMENTED: 501,
  UNPROCESSABLE_ENTITY: 422,
  // Eror Attributes
  EXPIRED_TOKEN: 2004
};

export const LIST_PROTOCOL_VPN = [
  {
    text: "PPTP",
    value: "pptp"
  },
  {
    text: "L2TP",
    value: "l2tp"
  },
  {
    text: "OpenVPN",
    value: "openvpn"
  }
];

export const LIST_PROTOCOL_OPENVPN = [
  {
    text: "TCP",
    value: "tcp"
  },
  {
    text: "UDP",
    value: "udp"
  }
];

export const USB_PORT_COUNT = 1;

export const MEDIASHARE_SERVER_STATUS = {
  DEFAULT: "-",
  FTP: "ftp",
  SAMBA: "samba",
  TORRENT: "torrent",
  WEBDAV: "webdav",
  TIMEMACHINE: "timemachine"
};

export const URL_HELP_GUIDE = {
  USE_ONLINE_HELP: true,
  URL_ONLINE: "http://quantum.humaxdigital.com/support/",
  URL_INAPP: "help_guide_content_card/"
};
export const LIST_VPN_TYPE = [
  {
    value: 1,
    text: "security_vpn_mode_server"
  },
  {
    value: 2,
    text: "security_vpn_mode_client"
  }
];

export const COMPLETE_DIALOG_TYPE = {
  COMPLETE: "complete",
  WARNING: "warning",
  FIRMWARE_UPDATE_MANDATORY: "firmwareUpdateMandatory"
};

export const CONFIRM_DIALOG_TYPE = {
  CONFIRM: "confirm",
  CHOOSE_FIRMWARE: "chooseFirmware",
  CONFIRM_BEFORE_UPGRADE: "confirmBeforUpgrade",
  FIRMWARE_UPDATE_SAVE: "firmwareUpdateSave"
};

export const SPINNER_DIALOG_TYPE = {
  LOADING: "loading",
  UPGRADE_FIRMWARE: "upgradeFirmware",
  RESTART_AFTER_UPGRADE: "restartAfterUpgrade"
};

export const PARENTAL_CONSTANT = {
  DAY: {
    SUN: "sun",
    MON: "mon",
    TUE: "tue",
    WED: "wed",
    THU: "thu",
    FRI: "fri",
    SAT: "sat"
  },
  BLOCK_TYPE: {
    BLACK_LIST: "blacklist",
    ALWAYS: "always"
  },
  PASSWORD_LENGTH: 4,
  PASSWORD_INITIAL: "!@#$"
};

export const PARENTAL_FILTER_DEFINE = {
  USER_DEFINED: "User Defined",
  USER_DEFINED_PARENT: "User Define",
  TYPE: {
    URL: "url",
    KEY_WORD: "keyword"
  },
  USER_DEFINE_MAX_RULE: 30
};

export const PARENTAL_SCHEDULE = {
  TYPE: {
    NONE: "none",
    ALWAYS: "always",
    SPECIFIC: "specific"
  }
};
export const DDNS_MODEL = {
  DYNDNS: 0,
  NOIP: 1,
  DUCKDNS: 2,
  HUMAXDNS: 3
};

export const IPTV_DISABLED_VLAN = [
  "KT",
  "SKB/LGU+",
  "AIS",
  "Singapore-ExStream",
  "Malaysia-Unifi",
  "Bridge"
];

export const LIST_UTILITY = [
  {
    text: "advanced_diagnostics_page_ping_test_lts_utility",
    value: "ping"
  },
  {
    text: "advanced_diagnostics_page_traceroute_lts_utility",
    value: "traceroute"
  }
];

export const DIAGNOSTICS = {
  PING: "ping",
  TRACEROUTE: "traceroute",
  REQUESTED: "requested"
};

export const WAN_IPV6_TYPE = {
  DHCPV6: "dhcpv6",
  SLAAC: "slaac",
  STATIC_IP: "static"
};

export const MODE_IPV6 = {
  DHCPV6: "dhcpv6",
  SLAAC: "slaac"
};

export const LIST_LED_MODE = [
  {
    text: "security_firewall_card_list_led_mod_always_on",
    value: "always"
  },
  {
    text: "security_firewall_card_list_led_mod_always_off",
    value: "power"
  },
  {
    text: "security_firewall_card_list_led_mod_specific_times",
    value: "specific"
  }
];

export const FIRMWARE_CHOOSE_OPTION = {
  LASTEST_VERSION: 0,
  SELECTED_VERSION: 1
};

export const UPGRADE_FIRMWARE_TYPE = {
  SERVER: "server",
  MANUAL: "manual"
};

export const WIZARD_BUTTON_TYPE = {
  START: "start",
  SUMMARY: "sumary"
};

export const REGIONS = [
  {
    text: "Austria",
    value: "AT"
  },
  {
    text: "Bahrain",
    value: "BH"
  },
  {
    text: "Denmark",
    value: "DK"
  },
  {
    text: "Finland",
    value: "FI"
  },
  {
    text: "France",
    value: "FR"
  },
  {
    text: "Germany",
    value: "DE"
  },
  {
    text: "Iran",
    value: "IR"
  },
  {
    text: "Italia",
    value: "IT"
  },
  {
    text: "Japan",
    value: "JP"
  },
  {
    text: "Kuwait",
    value: "KW"
  },
  {
    text: "Norway",
    value: "NO"
  },
  {
    text: "Oman",
    value: "OM"
  },
  {
    text: "Portugal",
    value: "PT"
  },
  {
    text: "Qatar",
    value: "QA"
  },
  {
    text: "Republic of Korea",
    value: "KR"
  },
  {
    text: "Russia",
    value: "RU"
  },
  {
    text: "Saudi Arabia",
    value: "SA"
  },
  {
    text: "Spain",
    value: "ES"
  },
  {
    text: "Sweden",
    value: "SE"
  },
  {
    text: "Thailand",
    value: "TH"
  },
  {
    text: "Turkey",
    value: "TR"
  },
  {
    text: "UAE",
    value: "AE"
  },
  {
    text: "United Kingdom",
    value: "GB"
  },
  {
    text: "United States of America",
    value: "US"
  },
  {
    text: "Vietnam",
    value: "VN"
  },
  {
    text: "-",
    value: "XX"
  }
];
export const WIZARD_PROMISE_NAME = {
  EMPTY: "",
  CHANGE_PASSWORD: "changePassword",
  UPDATE_Q_MODE: "updateQMode",
  UPDATE_SPECIFIC_WAN: "updateSpecificWan",
  UPDATE_SSID: "updateSSID"
};

export const DEFAULT_WIZARD_HEADER = 5;

export const CWORD = [
  "60ee0bc62638fccf2d37ac27a634a9e9",
  "68e2d83709f317938b51e53f7552ed04",
  "f4c9385f1902f7334b00b9b4ecd164de",
  "df491a4de50739fa9cffdbd4e3f4b4bb",
  "ef56b0b0ddb93c2885892c06be830c68",
  "fe4c0f30aa359c41d9f9a5f69c8c4192",
  "cbf4e0b7971051760907c327e975f4e5",
  "ea9e801b0d806f2398bd0c7fe3f3f0cd",
  "609a8f6f218fdfe6f955e19f818ec050",
  "cbf4e0b7971051760907c327e975f4e5",
  "8cb554127837a4002338c10a299289fb",
  "28f9b1cae5ae23caa8471696342f6f0c",
  "74e04ddb55ce3825f65ebec374ef8f0d",
  "567904efe9e64d9faf3e41ef402cb568",
  "7edabf994b76a00cbc60c95af337db8f",
  "639849f6b368019778991b32434354fc",
  "7edabf994b76a00cbc60c95af337db8f",
  "dd8fc45d87f91c6f9a9f43a3f355a94a",
  "eb5c1399a871211c7e7ed732d15e3a8b",
  "8cb554127837a4002338c10a299289fb",
  "0b8263d341de01f741e4deadfb18f9eb",
  "87fa4eaaf3698e1b1e2caadabbc8ca60",
  "327a6c4304ad5938eaf0efb6cc3e53dc",
  "841a2d689ad86bd1611447453c22c6fc",
  "ceb20772e0c9d240c75eb26b0e37abee",
  "a3e2a6cbf4437e50816a60a64375490e",
  "bc8fba5b68a7babc05ec51771bf6be21",
  "68934a3e9455fa72420237eb05902327",
  "c9fab33e9458412c527c3fe8a13ee37d",
  "2fc01ec765ec0cb3dcc559126de20b30",
  "fcc790c72a86190de1b549d0ddc6f55c",
  "918b81db5e91d031548b963c93845e5b",
  "9dfc8dce7280fd49fc6e7bf0436ed325",
  "ea82410c7a9991816b5eeeebe195e20a",
  "fb81c91eb92d6cb64aeb64c3f37ef2c4",
  "8d45c85b51b27a04ad7fdfc3f126f9f8",
  "70dda5dfb8053dc6d1c492574bce9bfd",
  "b9b83bad6bd2b4f7c40109304cf580e1",
  "981c1e7b3795da18687613fbd66d4954",
  "e170e3a15923188224c1c2bd1477d451",
  "fb81c91eb92d6cb64aeb64c3f37ef2c4",
  "cb15e32f389b7af9b285a63ca1044651",
  "632a2406bbcbcd553eec45ac14b40a0a",
  "e7b95b49658278100801c88833a52522",
  "6d4db5ff0c117864a02827bad3c361b9",
  "8b373710bcf876edd91f281e50ed58ab",
  "508c75c8507a2ae5223dfd2faeb98122",
  "97f014516561ef487ec368d6158eb3f4",
  "23678db5efde9ab46bce8c23a6d91b50",
  "2d6b0cefb06fd579a62bf56f02b6c2b3",
  "f1bdf5ed1d7ad7ede4e3809bd35644b0",
  "3ddaeb82fbba964fb3461d4e4f1342eb",
  "c9507f538a6e79c9bd6229981d6e05a3",
  "9e925e9341b490bfd3b4c4ca3b0c1ef2",
  "125097a929a62998c06340ea9ef43d77",
  "a557264a7d6c783f6fb57fb7d0b9d6b0",
  "eba478647c77836e50de44b323564bdb",
  "45fe7e5529d283851d93b74536e095a0",
  "56609ab6ba04048adc2cbfafbe745e10",
  "d938ad5cbe68bec494fbbf4463ad031d",
  "9bbd993d9da7df60b3fd4a4ed721b082",
  "a6ab62e9da89b20d720c70602624bfc2",
  "51037a4a37730f52c8732586d3aaa316",
  "7c4f29407893c334a6cb7a87bf045c0d",
  "3b7770f7743e8f01f0fd807f304a21d0",
  "29d233ae0b83eff6e5fbd67134b88717",
  "8d45c85b51b27a04ad7fdfc3f126f9f8",
  "9aa91f81de7610b371dd0e6fe4168b01",
  "9f27410725ab8cc8854a2769c7a516b8",
  "6ee6a213cb02554a63b1867143572e70",
  "918b81db5e91d031548b963c93845e5b",
  "3767b450824877f2b8f284f7a5625440",
  "81513effdf5790b79549208838404407",
  "7aea2552dfe7eb84b9443b6fc9ba6e01",
  "d8735f7489c94f42f508d7eb1c249584",
  "fde27e470207e146b29b8906826589cb",
  "2a2d595e6ed9a0b24f027f2b63b134d6",
  "99e0d947e01bbc0a507a1127dc2135b1",
  "6758fcdc0da017540d11889c22bb5a6e",
  "ab1991b4286f7e79720fe0d4011789c8",
  "28f9b1cae5ae23caa8471696342f6f0c",
  "f5b75010ea8a54b96f8fe7dafac65c18",
  "2570c919f5ef1d7091f0f66d54dac974",
  "ada15bd1a5ddf0b790ae1dcfd05a1e70",
  "eb88d7636980738cd0522ea69e212905",
  "83ab982dd08483187289a75163dc50fe",
  "8ac20bf5803e6067a65165d9df51a8e7",
  "7c4f29407893c334a6cb7a87bf045c0d",
  "67942503875c1ae74e4b5b80a0dade01",
  "d74fdde2944f475adc4a85e349d4ee7b",
  "163ccb6353c3b5f4f03cda0f1c5225ba",
  "6b1628b016dff46e6fa35684be6acc96",
  "de1b2a7baf7850243db71c4abd4e5a39",
  "5eda0ea98768e91b815fa6667e4f0178",
  "23ec24c5ca59000543cee1dfded0cbea",
  "ea9e801b0d806f2398bd0c7fe3f3f0cd",
  "35393c24384b8862798716628f7bc6f4",
  "28b26be59c986170c572133aaace31c2",
  "c2bfd01762cfbe4e34cc97b9769b4238",
  "22811dd94d65037ef86535740b98dec8",
  "acaa16770db76c1ffb9cee51c3cabfcf",
  "7516c3b35580b3490248629cff5e498c",
  "b04ab37e571600800864f7a311e2a386",
  "7e25b972e192b01004b62346ee9975a5",
  "2764ca9d34e90313978d044f27ae433b",
  "660cb6fe7437d4b40e4a04b706b93f70",
  "87a429872c7faee7e8bc9268d5bf548e",
  "31c13f47ad87dd7baa2d558a91e0fbb9",
  "e6ec529ba185279aa0adcf93e645c7cd",
  "21a361d96e3e13f5f109748c2a9d2434",
  "85814ce7d88361ec8eb8e07294043bc3",
  "a5fdad9de7faf3a0492812b9cb818d85",
  "0b8263d341de01f741e4deadfb18f9eb",
  "0cb47aeb6e5f9323f0969e628c4e59f5",
  "23a58bf9274bedb19375e527a0744fa9",
  "7e25b972e192b01004b62346ee9975a5",
  "b9d27d6b3d1915aacd5226b9d702bdbb",
  "6758fcdc0da017540d11889c22bb5a6e",
  "e2704f30f596dbe4e22d1d443b10e004",
  "da4f0053a5c13882268852ae2da2e466",
  "1562eb3f6d9c5ac7e159c04a96ff4dfe",
  "a94aa000f9a94cc51775bd5eac97c926",
  "1e4483e833025ac10e6184e75cb2d19d",
  "a957a3153eb7126b1c5f8b6aac35de53",
  "731b886d80d2ea138da54d30f43b2005",
  "a850c17cba5eb16b0d3d40a106333bd5",
  "7516c3b35580b3490248629cff5e498c",
  "d508fe45cecaf653904a0e774084bb5c",
  "18ccf61d533b600bbf5a963359223fe4",
  "f4d3b5a1116ded3facefb8353d0bd5ba",
  "28b26be59c986170c572133aaace31c2",
  "d5ca322453f2986b752e58b11af83d96",
  "37b19816109a32106d109e83bbb3c97d",
  "0423fa423baf1ea8139f6662869faf2f",
  "8ab8a4dfab57b4618331ffc958ebb4ec",
  "85814ce7d88361ec8eb8e07294043bc3",
  "273b9ae535de53399c86a9b83148a8ed",
  "4c9184f37cff01bcdc32dc486ec36961",
  "8ee2027983915ec78acc45027d874316",
  "1cba77c39b4d0a81024a7aada3655a28",
  "de1b2a7baf7850243db71c4abd4e5a39",
  "608f0b988db4a96066af7dd8870de96c",
  "06a224da9e61bee19ec9eef88b95f934",
  "df55340f75b5da454e1c189d56d7f31b",
  "8c728e685ddde9f7fbbc452155e29639",
  "2570c919f5ef1d7091f0f66d54dac974",
  "dce7c4174ce9323904a934a486c41288",
  "573ce5969e9884d49d4fab77b09a306a",
  "d5ca322453f2986b752e58b11af83d96",
  "eb88d7636980738cd0522ea69e212905",
  "e7e94d9ef1edaf2c6c55e9966b551295",
  "762f8817ab6af0971fe330dbf46a359a",
  "d8a48e3f0e1322d53d401e3dcb3360db",
  "c1940aeeb9693a02e28c52eb85ce261c",
  "d74fdde2944f475adc4a85e349d4ee7b",
  "b6a5d96a4e99b63723ab54ddb471baad",
  "6b157916b43b09df5a22f658ccb92b64",
  "bec670e5a55424d840db8636ecc28828",
  "4a6cbcd66d270792b89f50771604d093",
  "07202a7e6cbfbabe27abba87989f807e",
  "d60db28d94d538bbb249dcc7f2273ab1",
  "123402c04dcfb6625f688f771a5fc05d",
  "cd69b4957f06cd818d7bf3d61980e291",
  "be1ab1632e4285edc3733b142935c60b",
  "2bda2998d9b0ee197da142a0447f6725",
  "ba535ef5a9f7b8bc875812bb081286bb",
  "e9f40e1f1d1658681dad2dac4ae0971e",
  "eabe04e738cfb621f819e4e8f9489234",
  "aa2d6e4f578eb0cfaba23beef76c2194",
  "126ac4b07f93bc4f7bed426f5e978c16",
  "f43dff9a0dc54f0643d0c6d7971635f0",
  "ccaaac957ec37bde4c9993a26a064730",
  "2feaaf89c21770ea5c21196bc33848dd",
  "07cf4f8f5d8b76282917320715dda2ad",
  "1ffd9e753c8054cc61456ac7fac1ac89",
  "6050ce63e4bce6764cb34cac51fb44d1",
  "327a6c4304ad5938eaf0efb6cc3e53dc",
  "b82c91e2103d0a495c099f0a12f66363",
  "41d1de28e96dc1cde568d3b068fa17bb",
  "cad1c068cb62b0681fe4c33d1db1bad6",
  "de1b2a7baf7850243db71c4abd4e5a39",
  "75e52a0ecfafeda17a34fc60111c1f0b",
  "fc7e987f23de5bd6562b7c0063cad659",
  "126ac4b07f93bc4f7bed426f5e978c16",
  "fcc790c72a86190de1b549d0ddc6f55c",
  "72792fa10d4ca61295194377da0bcc05",
  "821f03288846297c2cf43c34766a38f7",
  "faec47e96bfb066b7c4b8c502dc3f649",
  "78b6367af86e03f19809449e2c365ff5",
  "015f28b9df1bdd36427dd976fb73b29d",
  "755f85c2723bb39381c7379a604160d8"
];

export const MENU_TYPE = {
  MAIN: "mainMenu",
  SUB: "subMenu",
  MAP: "map"
};

export const MESH_TYPE = {
  DEFAULT: "-",
  STATION: "sta",
  SLAVE: "slave"
};
