export const dictionaries = {
  messages: {
    // common messages
    required: () => "error_message_common_require"
  },
  custom: {
    // custom messages, matching with validateprovider' name
    "ipv4-validator": {
      required: () => "",
      numeric: () => ""
    },
    "alphanumeric-mac-validator": {
      regex: () => "common_ui_alphanumeric_mac_error_format",
      ExistedMac: () => "common_ui_alphanumeric_mac_error_duplicate"
    },
    "select-device-single-validator": {
      required: () => "common_ui_inputbox_require",
      regex: () => "page_error_invalid_mac_address",
      RestricMac: () => "common_ui_alphanumeric_mac_error_multicast"
    },
    "change-pasword-ogirin-pasword-validator": {
      required: () => "common_ui_inputbox_require",
      isCurrentPWCorrect: () => "change_pass_error_password_is_not_correct"
    },
    "change-pasword-new-pasword-validator": {
      required: () => "common_ui_inputbox_require",
      isCurrentPWCorrect: () => ""
    },
    "change-pasword-re-new-pasword-validator": {
      required: () => "common_ui_inputbox_require",
      isCurrentPWCorrect: () => ""
    },
    "port-triggering-validator-left": {
      required: () => "",
      numeric: () => "",
      reverved: () => "advanced_port_triggering_page_col_reserved_port_msg"
    },
    "port-triggering-validator-right": {
      required: () => "",
      numeric: () => "",
      reverved: () => ""
    },
    "repeater-step1-validator": {
      required: () => "error_message_common_password_require",
      validLength: () => "error_message_common_password_minlength"
    },
    "repeater-step2-validator": {
      required: () => "error_message_common_password_require",
      validLength: () => "error_message_common_password_minlength"
    },
    "repeater-step2-ssid-validator": {
      required: () => "error_message_common_require"
    },
    "manual-scan-input-validator": {
      required: () => "error_message_common_require"
    },
    "account-setting-input-value-lable-validator": {
      required: () => "common_ui_inputbox_require",
      exitedId: () => "account_setting_card_duplicate",
      sensitiveId: () => "account_setting_card_disallowed_account"
    },
    "vpn-server-input-value-lable-validator": {
      required: () => "common_ui_inputbox_require",
      exitedId: () => "account_setting_vpn_duplicate",
      sensitiveId: () => "account_setting_card_disallowed_account"
    },
    "account-setting-password-input-validator": {
      required: () => "common_ui_inputbox_require"
    },
    "select-folder-validator": {
      required: () => "",
      exitedPath: () => "mediashare_usb_network_folder_duplicate_folder"
    },
    "window-network-input-validator": {
      required: () => "error_message_common_require"
    },
    "parental-code-new-password-validator": {
      required: () => "common_ui_inputbox_require",
      validLength: () => "change_parental_control_code_min_length"
    },
    "parental-code-re-password-validator": {
      required: () => "common_ui_inputbox_require",
      validLength: () => "change_parental_control_code_min_length",
      matchPassword: () => "parental_control_parental_code_does_not_match"
    },
    "select-device-multiple-validator": {
      required: () => "",
      regex: () => "page_error_invalid_mac_address",
      RestricMac: () => "common_ui_alphanumeric_mac_error_multicast"
    },
    "host-name-validator": {
      required: () => "common_ui_inputbox_require",
      isValidHost: () => "advanced_ddns_wol_invalid_hostname_msg",
      isValidHyphen: () => "advanced_ddns_wol_invalid_hostname_msg_by_hyphen",
      isNumericString: () => "advanced_ddns_wol_invalid_hostname_msg_by_numeric"
    },
    "domain-name-validator": {
      required: () => "common_ui_inputbox_require",
      isValidDomain: () => "advanced_ddns_wol_invalid_hostname_msg",
      isValidHyphen: () => "advanced_ddns_wol_invalid_hostname_msg_by_hyphen",
      isNumericString: () => "advanced_ddns_wol_invalid_hostname_msg_by_numeric"
    },
    "select-single-device-mac-filtering-validator": {
      required: () => "common_ui_inputbox_require",
      regex: () => "page_error_invalid_mac_address",
      RestricMac: () => "common_ui_alphanumeric_mac_error_multicast"
    },
    "wizad-change-pasword-ogirin-pasword-validator": {
      required: () => "common_ui_inputbox_require",
      isCurrentPWCorrect: () => "change_pass_error_password_is_not_correct"
    },
    "wizad-change-pasword-new-pasword-validator": {
      required: () => "common_ui_inputbox_require",
      isCurrentPWCorrect: () => ""
    },
    "wizad-change-pasword-re-new-pasword-validator": {
      required: () => "common_ui_inputbox_require",
      isCurrentPWCorrect: () => ""
    },
    "wizard-repeater-step2-password-validator": {
      required: () => "error_message_common_password_require",
      validLength: () => "error_message_common_password_minlength"
    },
    "wizard-wireless-setup-validator": {
      required: () => "common_ui_inputbox_require",
      validLength: () => "error_message_common_password_minlength"
    },
    "wizard-wireless-setup-ssid-validator": {
      required: () => "common_ui_inputbox_require"
    },
    "select-single-device-access-control-validator": {
      required: () => "common_ui_inputbox_require",
      regex: () => "page_error_invalid_mac_address",
      RestricMac: () => "common_ui_alphanumeric_mac_error_multicast"
    },
    "ip-port-filtering-input-value-label-validator": {
      required: () => "error_message_common_require"
    }
  }
};
