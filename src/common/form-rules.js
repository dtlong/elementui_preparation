import I18n from "@/plugins/i18n";
const EVENT = {
  BLUR: "blur",
  CHANGE: "change"
};
const formRules = {
  required(rule, value, callback) {
    if (!value) {
      return callback(new Error(I18n.t("error_message_common_require")));
    } else {
      callback();
    }
  },
  checkRegex(rule, value, callback) {
    const regex = /^([0-9][-])([0-9])$/;
    if (value && !regex.test(value)) {
      return callback(
        new Error(I18n.t("advanced_ddns_wol_invalid_hostname_msg_by_numeric"))
      );
    } else {
      callback();
    }
  },
  invalidIPV4(rule, value, callback) {
    const regex = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    if (value && !regex.test(value)) {
      return callback(
        new Error(I18n.t("advanced_ddns_wol_invalid_hostname_msg"))
      );
    } else {
      callback();
    }
  },
  exitedIP(rule, value, callback) {
    const vListIP = rule && rule.listIP ? rule.listIP : [];
    let valid = true;
    for (let i = 0; i < vListIP.length; i++) {
      if (value === vListIP[i].ipAddress) {
        valid = false;
      }
    }
    if (!valid) {
      return callback(new Error(I18n.t("This IPAddress has been exited.")));
    } else {
      callback();
    }
  }
};

export const FORM_RULE = {
  COMMON_REQUIRE: {
    validator: formRules.required,
    trigger: [EVENT.BLUR]
  },
  NUMERIC_REQUIRE: {
    validator: formRules.checkRegex,
    trigger: [EVENT.CHANGE, EVENT.BLUR]
  },
  INVALID_IP_V4: {
    validator: formRules.invalidIPV4,
    trigger: [EVENT.BLUR]
  },
  EXITED_IP: {
    validator: formRules.exitedIP,
    trigger: [EVENT.BLUR]
  }
};
