const validate = {
  existedMac(value, [list, index]) {
    if (!list) {
      return true;
    }
    const vMacAddress = value.trim();
    const vListmac = Array.from(list);
    const vIndex = index === null || index === undefined ? -1 : index;
    if (vListmac.length === 0) {
      return true;
    } else {
      for (let i = 0; i < vListmac.length; i++) {
        if (vIndex === i) {
          continue;
        }
        if (
          vMacAddress.toUpperCase() === vListmac[i].macAddress.toUpperCase()
        ) {
          return false;
        }
      }
    }
    return true;
  },
  validLength(value, [min]) {
    if (value.length < min) {
      return false;
    }
    return true;
  },
  matchPassword(value, [isCheck, referPassword, isCheckSame]) {
    if (isCheck === false) {
      return true;
    }
    if (referPassword === null || referPassword === undefined) {
      return true;
    }
    if (isCheckSame) {
      return value === referPassword ? false : true;
    } else {
      return value === referPassword ? true : false;
    }
  },
  isCurrentPWCorrect(value, [isMatched]) {
    if (isMatched === undefined || isMatched === null) {
      return true;
    }
    return !!isMatched;
  },
  reservedPort(value, [list, protocol, endPort]) {
    if (list === null || list === undefined) {
      return true;
    }
    if (endPort === null || endPort === undefined) {
      return true;
    } else if (endPort === "") {
      return true;
    }
    let vProtocol = "";
    let vStart = value;
    let vEnd = endPort;
    if (vEnd < vStart) {
      vEnd = vStart;
    }
    const vList = Array.from(list);
    if (protocol === null || protocol === undefined) {
      vProtocol = "tcp";
    } else {
      vProtocol = protocol;
    }
    for (let n in vList) {
      if (
        vList[n].value &&
        vList[n].limitPort &&
        vList[n].value === vProtocol
      ) {
        for (let port in vList[n].limitPort) {
          if (
            vList[n].limitPort[port] >= vStart &&
            vList[n].limitPort[port] <= vEnd
          ) {
            return false;
          }
        }
        break;
      }
    }
    return true;
  },
  exitedId(value, [data]) {
    if (data === null || data === undefined) {
      return true;
    }
    if (data.length === 0) {
      return true;
    }
    for (let i = 0; i < data.length; i++) {
      if (data[i].userName && data[i].userName === value) {
        return false;
      }
    }
    return true;
  },
  sensitiveId(value, [data]) {
    if (data === null || data === undefined) {
      return true;
    }
    if (data.length === 0) {
      return true;
    }
    for (let i = 0; i < data.length; i++) {
      if (data[i].toUpperCase() === value.toUpperCase()) {
        return false;
      }
    }
    return true;
  },
  exitedPath(value, [listPath]) {
    if (listPath === null || listPath === undefined) {
      return true;
    }
    if (listPath.length === 0) {
      return true;
    }
    for (let i = 0; i < listPath.length; i++) {
      if (listPath[i] && listPath[i].toUpperCase() === value.toUpperCase()) {
        return false;
      }
    }
    return true;
  },
  restricMac(value, [regex]) {
    if (regex === null || regex === undefined || regex === "") {
      return true;
    }
    const partern = new RegExp(regex);
    return !partern.test(value);
  },
  isValidHost(value) {
    let specialChar = false;
    const num = "{}[]()<>?_|~`!@#$%^&*+\"'\\.,:;=/ ";
    for (let i = 0; i < value.length; i += 1) {
      if (
        -1 != num.indexOf(value.charAt(i)) ||
        (value.charCodeAt(i) < 32 || value.charCodeAt(i) > 126)
      ) {
        specialChar = true;
      }
    }
    return !specialChar;
  },
  isValidHyphen(value) {
    if (value.charAt(0) === "-" || value.charAt(value.length - 1) === "-") {
      return false;
    }
    return true;
  },
  isValidDomain(value) {
    let specialChar = false;
    const num = "{}[]()<>?_|~`!@#$%^&*+\"'\\:;=/ ";
    for (let i = 0; i < value.length; i += 1) {
      if (
        -1 != num.indexOf(value.charAt(i)) ||
        (value.charCodeAt(i) < 32 || value.charCodeAt(i) > 126)
      ) {
        specialChar = true;
      }
    }
    return !specialChar;
  },
  isNumericString(value) {
    let isCharacterNumber = false;
    for (let i = 0; i < value.length; i += 1) {
      if (isNaN(parseInt(value.charAt(i), 10))) {
        isCharacterNumber = true;
        break;
      }
    }
    return isCharacterNumber;
  },
  isValidMac(value, regex) {
    let vRegex = /^([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})$/;
    if (regex === null || regex === undefined || regex === "") {
      vRegex = regex;
    }
    const partern = new RegExp(vRegex);
    return partern.test(value);
  }
};

export const VALID_RULES = [
  {
    id: "ValidMac",
    msg: "Invalid Mac Address",
    validate: validate.isValidMac
  },
  {
    id: "ExistedMac",
    msg: "common_ui_alphanumeric_mac_error_duplicate",
    validate: validate.existedMac
  },
  {
    id: "exitedId",
    msg: "account_setting_card_duplicate",
    validate: validate.exitedId
  },
  {
    id: "sensitiveId",
    msg: "account_setting_card_disallowed_account",
    validate: validate.sensitiveId
  },
  {
    id: "validLength",
    msg: "common_ui_password_input_error_minlength",
    validate: validate.validLength
  },
  {
    id: "matchPassword",
    msg: "change_pass_error_password_not_match",
    validate: validate.matchPassword
  },
  {
    id: "isCurrentPWCorrect",
    msg: "change_pass_error_password_is_not_correct",
    validate: validate.isCurrentPWCorrect
  },
  {
    id: "samePassword",
    msg: "error_new_password_same_as_current",
    validate: validate.matchPassword
  },
  {
    id: "RestricMac",
    msg: "common_ui_alphanumeric_mac_error_multicast",
    validate: validate.restricMac
  },
  {
    id: "isValidHost",
    msg: "advanced_ddns_wol_invalid_hostname_msg",
    validate: validate.isValidHost
  },
  {
    id: "isValidHyphen",
    msg: "advanced_ddns_wol_invalid_hostname_msg_by_hyphen",
    validate: validate.isValidHyphen
  },
  {
    id: "isValidDomain",
    msg: "advanced_ddns_wol_invalid_hostname_msg",
    validate: validate.isValidDomain
  },
  {
    id: "isNumericString",
    msg: "advanced_ddns_wol_invalid_hostname_msg_by_numeric",
    validate: validate.isNumericString
  },
  {
    id: "exitedPath",
    msg: "mediashare_usb_network_folder_duplicate_folder",
    validate: validate.exitedPath
  },
  {
    id: "reservedPort",
    msg: "advanced_port_triggering_page_col_reserved_port_msg",
    validate: validate.reservedPort
  }
];
